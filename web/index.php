<!DOCTYPE HTML SYSTEM>

<html>

<header>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-W9CB39X');</script>
<!-- End Google Tag Manager -->

<meta charset = "utf-8">

<title>Компания Руслада</title>
<link rel="icon" href="../rus.ico" type="image/x-icon">
 
<link href = "../css/style.css" rel = "stylesheet"  type = "text/css" media = "all">
<link  rel = "stylesheet" href = "../css/font_adaptive.css" type = "text/css" >

<script type="text/javascript" src = "../js/navigation.js"></script>
<script type="text/javascript" src = "../js/scripts.js"></script>
<script type="text/javascript" src = "../js/keyboard.js"></script>
<script type="text/javascript" src = "../js/top_slider.js"></script>
<script type="text/javascript" src = "../js/jquery-3.1.1.js"></script>

<meta name="yandex-verification" content="018d0299099abcf5" />     <!-- верификация на яндекс-мастере для живосайта -->

</header>


<body onload = "document.getElementById('slider_selector_1').className = 'slider_selector_active'; start_load()" style = "font-family: arial;">


		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W9CB39X"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->


		<a id = "goto_vk" style = "position: absolute; left: 20px; top: 20px; transition: 0.5s; z-index: 999;" href = "https://vk.com/ruslada_che" target = "_blank" >
			<img src = "images/vk.png" style = "width: 50px;" >
		</a>
		
		<a id = "tel" onclick = "load_feedback()" style = "position: absolute; left: 90px; background: #A92725; top: 20px; border-radius: 100%; transition: 0.5s; padding: 15px; z-index: 999;" target = "_blank" >
			<img src = "images/tel.png" style = "width: 30px;" >
		</a>
		
		
		<input type = "hidden" id = "count_children" value = "" style = "position: fixed; top: 40px; z-index: 999;">               <!--  буфер количества найденных товаров в поисковике  -->

		<div id = "load_modal_window" style = "position: fixed; left: 50%; top: 50%; width: 0px; height: 0px; z-index: 0; transition: 0.2s; -web-kit-transition: 0.2s;">    <!--  модальное окно  -->
		</div>
	
		<div style = "position: absolute; top: 0px; left: 0px; width: 100%; overflow: hidden;">										<!--  основной шаблон  -->
		
			<div id = "roof" style = "position: fixed; left: 80%; width: 20%; z-index: 0;">
				<div style = "width: 100%; float: right;">
					<center>
						<img 	src = "../images/roof.png" 
								style = "position: relative; width: 100%; margin-top: 0%;" 
								onmouseover = "this.src = '../images/roof_over.png'"
								onmouseout = "this.src = '../images/roof.png'"
								onmousedown = "this.src = '../images/roof_down.png'"
								onmouseup = "this.src = '../images/roof.png'"
								onclick = "document.location.hash = 'link_tmpl_0'" > 
								
						<!-- поисковик -->			
						<input 	id = "search" 
								type = "text" 
								value = "поиск" class = "search"
								onfocus	= "this.value = ''; this.style.color = '#888'; this.style.letterSpacing = '1px';"
								onblur = "setTimeout('search_erase()', 200); this.style.color = '#ccc'; this.style.letterSpacing = '3px';"
								onclick = "start_search_the_site(); "	>
								
						<div 	id = "search_resultat" class = "search_resultat">
						
						</div>
					</center>
				</div>
			</div>			
			
			
			
		
			<div style = "position: fixed; left: 82%; top: 25%; z-index: 26; background: rgba(255, 255, 255, 0.7); padding: 20px;">
			<table>
			
				<tr style = "vertical-align: top;">
					<td>
						<img src = "../images/kuadrat.png" style = "width: 20%;">	
					</td>
					<td>
						<a class = "font_days" href = "../images/action/Kolos_catalog_2018.pdf" download> Скачать каталог продукции</a><br><br>
					</td>
				</tr>
				<!-- <tr style = "vertical-align: top;">
					<td>
						<img src = "../images/kuadrat.png" style = "width: 20%;">	
					</td>
					<td>
						<a class = "font_days" href = "../images/action/ng_2019_2.pdf" download> Скачать каталог новогодних подарков</a><br><br>
					</td>
				</tr> -->
						
				<!-- ПРОИЗВОДИТЕЛИ -->
			
				<tr style = "vertical-align: top;">
					<td>
						<img src = "../images/kuadrat.png" style = "width: 20%;">
						<div class = "punkt_menu" id = "punkt_tmpl_2_all_creators" onclick = "look_all_creators()" name = "punkt_menu"></div>	
					</td>
					<td>
						<a class = "font_days" onclick = "look_all_creators()"> ПРОИЗВОДИТЕЛИ</a><br><br>
					</td>
				</tr>
				
				
				<!-- АССОРТИМЕНТ -->
				
				<tr style = "vertical-align: top;">
					<td>
						<img src = "../images/kuadrat.png" style = "width: 20%;">
						<div class = "punkt_menu" id = "punkt_tmpl_2" onclick = "goto_tmpl_2()" name = "punkt_menu"></div>	
					</td>
					<td>
						<a class = "font_days" onclick = "goto_tmpl_2()"> АССОРТИМЕНТ</a><br><br>
					</td>
				</tr>
				
				
				<!-- ЖУРНАЛ "АКЦИИ" -->
				
				<tr style = "vertical-align: top;">
					<td>
						<img src = "../images/kuadrat.png" style = "width: 20%;">
						<div class = "punkt_menu" id = "punkt_tmpl_1" onclick = "goto_tmpl_1()" name = "punkt_menu"></div>	
					</td>
					<td>
						<a class = "font_days" onclick = "goto_tmpl_1()"> ЖУРНАЛ "АКЦИИ"</a><br><br>
					</td>
				</tr>
				
				
				<!-- НОВОСТИ -->
				
				<tr style = "vertical-align: top;">
				
					<td>
						<img src = "../images/kuadrat.png" style = "width: 20%;">
						<div class = "punkt_menu" id = "punkt_news" onclick = "goto_news()" name = "punkt_menu"></div>	
					</td>
					<td>
						<a class = "font_days" onclick = "goto_news()"> НОВОСТИ</a>	<br><br>
					</td>
				</tr>
				
				
				<!-- О НАС -->
				
				<tr style = "vertical-align: top;">
					<td>
						<img src = "../images/kuadrat.png" style = "width: 20%;">
						<div class = "punkt_menu" id = "punkt_tmpl_3" onclick = "goto_tmpl_3()" name = "punkt_menu"></div>	
					</td>
					<td>
						<a class = "font_days" onclick = "goto_tmpl_3()"> О НАС</a>	<br><br>
					</td>
				</tr>
				
				
				<!-- ГДЕ КУПИТЬ -->
				
				<tr style = "vertical-align: top;">
					<td>
						<img src = "../images/kuadrat.png" style = "width: 20%;">
						<div class = "punkt_menu" id = "punkt_where_to_buy" onclick = "goto_where_to_buy()" name = "punkt_menu"></div>	
					</td>
					<td>
						<a class = "font_days" onclick = "goto_where_to_buy()"> ГДЕ КУПИТЬ </a>	<br><br>
					</td>
				</tr>
				
				
				<!-- КОНТАКТЫ -->
				
				<tr style = "vertical-align: top;">
					<td>
						<img src = "../images/kuadrat.png" style = "width: 20%;">
						<div class = "punkt_menu" id = "punkt_kontakty" onclick = "goto_kontakty()" name = "punkt_menu"></div>	
					</td>
					<td>
						<a class = "font_days" onclick = "goto_kontakty()"> КОНТАКТЫ</a>	<br><br>
					</td>
				</tr>
					
					
				
			</table>
			
			</div>
			
			
			
			
			<!--
			
			<div style = "position: fixed; left: 81%; top: 65%; z-index: 26; background: rgba(255, 255, 255, 0.7); padding: 10px;">
			
			<center>
			<a class = "font_days">НАШИ ПАРТНЕРЫ</a> <br><br>
			</center>
			<table>
				<tr style = "vertical-align: center;">
					<td style = "width: 35%;">
					<center>
						<a href = "http://sunny-land.ru/lokomotive/" target = "_blank" ><img src = "../images/logo/locomotiv.png" width = "100%" ><a>
					</center>
					</td>
					<td style = "width: 33%;">
					<center>
						<a href = "http://www.province74.ru" target = "_blank" ><img src = "../images/logo/province.png" width = "80%" ><a>
					</center>
					</td>
				</tr>
				<tr style = "vertical-align: center;">
					<td>
					<center>
						<a href = "http://grand-royal.ru" target = "_blank" ><img src = "../images/logo/grand_royal.png" width = "100%" ><a>
					</center>
					</td>
					<td style = "width: 27%;">
					<center>
						<a href = "http://sunny-land.ru/" target = "_blank" ><img src = "../images/logo/sunny_land.png" width = "90%" ><a>
					</center>
					</td>
				</tr>
			</table>
			
			</div>    
			
			-->
			
			
								
		
			
			
			<!-- открываем шаблон слайдера -->		
			
			<div style = "position: relative; width: 100%; left: 0%; top: 0; z-index: 6;">
			<center>   
			
		<!--	<div style = "position: relative; width: 100%; z-index: 10;">
					<img src = "../images/kuadrat.png" width = "38%">
					
					<img id = "baner_1" src = "../images/baners/baner_1.png" class = "slide" style = "left: 0%; width: 100%; z-index: 2;">
					<img id = "baner_2" src = "../images/baners/baner_2.png" class = "slide" style = "left: 0%; opacity: 0; width: 100%; z-index: 1;">
					<img id = "baner_3" src = "../images/baners/baner_3.png" class = "slide" style = "left: 0%; opacity: 0; width: 100%; z-index: 0;">
				</div>  -->
				
			<div style = "position: relative; overflow: hidden; left: 0%; top: 0%;">
				<img src = "../images/roof/proportions.png" style = "position: relative; width: 100%; left: 37%; top: 5%; z-index: 2;">
				<img id = "cloud_00" src = "../images/roof/cloud_00.png" style = "position: absolute; width: 100%; left: 0%; top: -15%; z-index: 1;">	
				
				<img id = "pastry_01" src = "../images/roof/pastry_01.png" style = "position: absolute; width: 100%; left: 0%; top: 0%; z-index: 9;">	
				<img id = "pastry_02" src = "../images/roof/pastry_02.png" style = "position: absolute; width: 100%; left: 0%; top: 0%; z-index: 4;">	 
				
				<img id = "logo" src = "../images/roof.png" style = "position: absolute; width: 30%; left: 35%; top: 5%; z-index: 2;">
				
				<img id = "cloud_01" src = "../images/roof/cloud_01.png" style = "position: absolute; width: 100%; left: 0%; top: 5%; z-index: 3;">
				<img id = "cloud_02" src = "../images/roof/cloud_02.png" style = "position: absolute; width: 60%; left: 50%; top: 20%; z-index: 4;"> 
				<img id = "cloud_03" src = "../images/roof/cloud_03.png" style = "position: absolute; width: 60%; left: -10%; top: 21%; z-index: 8;"> 
				
				<img id = "cloud_06" src = "../images/roof/cloud_03.png" style = "position: absolute; width: 60%; left: -10%; top: 20%; z-index: 6;">
				
				<img id = "cloud_04" src = "../images/roof/cloud_04.png" style = "position: absolute; width: 40%; left: 0%; top: 30%; z-index: 7;"> 
				<img id = "cloud_05" src = "../images/roof/cloud_05.png" style = "position: absolute; width: 30%; left: 30%; top: 30%; z-index: 8;">
				<img id = "cloud_06_2" src = "../images/roof/cloud_06.png" style = "position: absolute; width: 80%; left: 0%; top: 70%; z-index: 10;">  
			</div>
			
			</center>
			</div>
			

			
			<!-- закрываем шаблон слайдера -->
			
			
			<!--	<center>   -->
			
			
			
		<!--	<div class = "blank" style = " left: 0%; top: 0; z-index: 0;">
				<img src = "../images/logo/template.png" style = "position: relative; width: 100%;">
			</div> -->
			
			
			
			
			
			<input id = "coordinate" type = "hidden" value = "0" style = "position: fixed; top: 650px; left: 83%; z-index: 999;" />				<!-- координаты скролла -->
			
			<input id = "current_tmpl" type = "hidden" value = "6536543" style = "position: fixed; top: 680px; left: 83%; z-index: 999;" /> 		<!-- текущий раздел -->
			
			<input id = "section1" type = "hidden" value = "1" style = "position: fixed; top: 740px; left: 83%; z-index: 999;" /> 				<!-- буфер раздела 1 порядка - страницы: ассортимент, журналы, дипломы ... -->
			<input id = "lock1" type = "hidden" value = "0" style = "position: fixed; top: 740px; left: 93%; z-index: 999; width: 20px;" />     <!-- блокировка цикла 1 -->
			
			<input id = "section2" type = "hidden" value = "1" style = "position: fixed; top: 770px; left: 83%; z-index: 999;" />  			<!-- буфер раздела 2 порядка - задействованные производители данного раздела продукции -->
			<input id = "lock2" type = "hidden" value = "0" style = "position: fixed; top: 770px; left: 93%; z-index: 999; width: 20px;" />     <!-- блокировка цикла 2 -->
			
			
			<input id = "type_product" type = "hidden" value = "1" style = "position: fixed; top: 800px; left: 83%; z-index: 999;;" />			<!-- буфер раздела типа продукции - выбранный производитель -->
			<input id = "lock_type_product" type = "hidden" value = "0" style = "position: fixed; top: 800px; left: 93%; z-index: 999; width: 20px;" />     <!-- блокировка цикла типа -->
			
			
			<input id = "section3" type = "hidden" value = "1" style = "position: fixed; top: 830px; left: 83%; z-index: 999;" />				<!-- буфер раздела 3 порядка - выбранный производитель -->
			<input id = "lock3" type = "hidden" value = "0" style = "position: fixed; top: 830px; left: 93%; z-index: 999; width: 20px;" />     <!-- блокировка цикла 3 -->
			
			<input id = "section4" type = "hidden" value = "0" style = "position: fixed; top: 860px; left: 83%; z-index: 999;" />				<!-- буфер раздела 4 порядка - номер страницы набора товаров-->
			
			<input id = "section5" type = "hidden" value = "0" style = "position: fixed; top: 890px; left: 83%; z-index: 999;" />				<!-- буфер раздела 5 порядка - выбранный товар -->
			<input id = "lock5" type = "hidden" value = "0" style = "position: fixed; top: 890px; left: 93%; z-index: 999; width: 20px;" />     <!-- блокировка цикла 5 -->
			
			
			
			<div id = "roof_baner" class = "blank" style = "width: 80%; height: 100%; z-index: 6;">					<!-- верхний банер -->
				<input type = "hidden" id = "number_slide"><input type = "hidden" id = "limit_number_slide">			

				<center>

					<nobr>
						
						<div style = "position: relative; width: 100%; overflow: hidden;">
							<img style = "position: relative; width: 100%;" src = "../images/roof/baner_proporcions.png">
							
							<div id = "slide_4" class = "slide_style" style = "left: -100%; z-index: 0;">
								<img src = "../images/roof/sam_samolet.jpg" style = "width: 100%; z-index: 0;">
							</div>
							
							<div id = "slide_5" class = "slide_style" style = "left: -100%; z-index: 3;">
								<img src = "../images/roof/entrevista.jpg" style = "width: 100%; z-index: 0;">
							</div>
							
							<div id = "slide_1" class = "slide_style" style = "left: 0%; z-index: 5;">
								<img src = "../images/roof/nabor_bordo.jpg" style = "width: 100%; z-index: 0;">
							</div>
							
							<div id = "slide_2" class = "slide_style" style = "left: 100%; z-index: 2;">
								<img src = "../images/roof/karamel_79.jpg" style = "width: 100%; z-index: 0;">
							</div>
							
							<div id = "slide_3" class = "slide_style" style = "left: 100%; z-index: 0;">
								<img src = "../images/roof/iris.jpg" style = "width: 100%; z-index: 0;">
							</div>
						</div>
						
					</nobr>
					
					<br><br>
					<table>
						<tr>
							<td class = "select_td"> 
								<div id = "slider_selector_1"  class = "slider_selector_active" onclick = "limit_number_slide = 1; select_slide(); reset_class_slider_selector(); this.className = 'slider_selector_active';"></div>
							</td>
							<td class = "select_td">
								<div id = "slider_selector_2" class = "slider_selector" onclick = "limit_number_slide = 2; select_slide(); reset_class_slider_selector(); this.className = 'slider_selector_active';"></div>
							</td>
							<td class = "select_td">
								<div id = "slider_selector_3" class = "slider_selector" onclick = "limit_number_slide = 3; select_slide(); reset_class_slider_selector(); this.className = 'slider_selector_active';"></div>
							</td>
							<td class = "select_td">
								<div id = "slider_selector_4" class = "slider_selector" onclick = "limit_number_slide = 4; select_slide(); reset_class_slider_selector(); this.className = 'slider_selector_active';"></div>
							</td>
							<td class = "select_td">
								<div id = "slider_selector_5" class = "slider_selector" onclick = "limit_number_slide = 5; select_slide(); reset_class_slider_selector(); this.className = 'slider_selector_active';"></div>
							</td>
						</tr>
					</table>
					<br>
					<input type = "button" class = "play_stop" value = "STOP" id = "stop_slider" onclick = "stop_slider(); document.getElementById('play_slider').style.display = 'block'; this.style.display = 'none';"> 				
					<input type = "button" class = "play_stop" value = "PLAY" id = "play_slider" onclick = "play_slider(); document.getElementById('stop_slider').style.display = 'block'; this.style.display = 'none';" style = "display: none;">
				
				</center>
				
			</div>
			
			


			<input id = "kind_of_products" type = "hidden" value = "confectionery" style = "position: fixed; top: 0px; left: 0%; z-index: 25;" />				<!-- текущий вид продукции -->
							
	
			<script>
				function hidde_all_kind_of_products()
					{
						document.getElementById("all_kind_of_products").style.display = "none";				// закрываем вкладку выбора кондитерки и консервации
					}
					
				function unhidde_all_kind_of_products()
					{
						document.getElementById("all_kind_of_products").style.display = "block";             // открываем вкладку выбора кондитерки и консервации
					}	
			</script>
			
			<div id = "conservation" >
			</div>
			
			<div  id = "tmpl_2"	class = "blank" >   						<!--блок каталог продукции-->
				<div style = "position: absolute; width: 8%; right: 10px; top: 10px;">
					<a href = "../images/action/Kolos_catalog_2018.pdf" download>
						<img 	src = "../images/action/Kolos_catalog_2018.jpg" 
								style = "position: relative; padding: 5px; width: 100%; box-shadow: 0 0 5px rgba(0,0,0,0.3);"
								alt = "Скачать каталог продукции"
								title = "Скачать каталог продукции">
					</a>
					<!--	<a href = "../images/action/ng_2019_2.pdf" download>
						<img 	src = "../images/action/ng_2019.jpg" 
								style = "position: relative; padding: 5px; width: 54%; box-shadow: 0 0 5px rgba(0,0,0,0.3);"
								alt = "Скачать каталог продукции"
								title = "Скачать каталог продукции"> -->
					</a> 
				</div>
			<center>
			
				<!--переключатель вида продукции-->
			
				<div id = "name_kind_of_products" name = "name_kind_of_products" class = "header_block kind_of_products_selector" style = "position: absolute; background: #fff; float: right; z-index: 100;" onmouseover = "unhidde_all_kind_of_products()" onmouseout = "hidde_all_kind_of_products()">
				
					<a id = "select_kind_of_products">Кондитерcкие<br> изделия</a><img src = "../images/selector_deploy.png" style = "width: 20px;">
					
					<div id = "all_kind_of_products" style = "display: none;">
						<br><a onclick = "unlock(); document.location.hash = 'link_tmpl_2'; hidde_all_kind_of_products();" >Кондитерcкие<br>изделия</a><br><br>
						<a onclick = "unlock(); document.location.hash = 'link_conservation'; hidde_all_kind_of_products();" >Конcервация<br>и бакалея</a><br><br>
						<!-- <a onclick = "unlock(); document.location.hash = 'link_new_year_products'; hidde_all_kind_of_products();" >Новогодняя<br>продукция</a><br><br> -->
					</div>
					
				</div> 
			
				
				<br><br>
				
				<input id = "section_production" type = "button" class = "header_block  section_creators" onmouseover = "this.style.cursor = 'pointer'" onclick = "back_general_catalog()" value = "КАТАЛОГ ПРОДУКЦИИ"  >
				<input id = "creators_production" type = "button" class = "header_block  section_creators" onmouseover = "this.style.cursor = 'pointer'" onclick = "look_all_creators()" value = "ПРОИЗВОДИТЕЛИ" >
				<br><br>
				
				<input type = "hidden" id = "button_type_margen" name = "button_type_margen" value = "0"> 			<!--буфер обмена для смещения клавишного ряда типов продукции-->
				<input type = "hidden" id = "button_type_position" name = "button_type_position" value = "0"> 				<!--буфер обмена для отступа клавиши-->
				
				<table>					<!--хлебные крошки-->
					<tr>
						<td>
							<div id = "root" >
							<a class = "font_days" onmouseover = "this.style.cursor = 'pointer'" onclick = "back_general_catalog()">каталог /</a>
							</div>
						</td>
						<td>
							<div class = "font_days" id = "navigation_logo_creator">								
							</div>
						</td>
						<td>
							<div class = "font_days" id = "navigation_produkciya">								
							</div>
						</td>
						<td>
							<div class = "font_days" id = "page_produkciya">								
							</div>
						</td>
					</tr>
				</table>
				
				<br>
				
				<div id = "catalog" style = "position: relative; width: 90%; ">         <!--изменяемый контент каталога-->
					
				</div>
			</center>
			</div>
			
			
			
			
			<div  id = "tmpl_1" 	class = "blank" >   						<!--блок о ЖУРНАЛ АКЦИЙ-->
				<center>
				<div class = "header_block">
					ЖУРНАЛ "АКЦИИ"
				</div>
				<input type = "hidden" id = "define_load_journal_action" value = "0">
				<br>
					<div id = "load_journal_action" style = "width: 95%; padding-top: 20px; padding-bottom: 20px;">    										
					</div>
					
					<div id = "on_of_load_journal_action">
						<a onmouseover = "this.style.cursor = 'pointer'" onclick = "load_journal_action()">
							<img src = "../images/deploy.png" style = "height: 40px;">
						</a>
					</div>
				
				</center>
			</div>
			
			
			
			<div  id = "tmpl_news" 	class = "blank" >   						<!--блок новости-->
				<center>
				<div class = "header_block">
					НОВОСТИ
				</div>
				<input type = "hidden" id = "define_load_news" value = "0">
				<br>
					
					
					
					<div style = "width: 100%;">
					
					<script type="text/javascript" src="//vk.com/js/api/openapi.js?152"></script>

					<!-- VK Widget -->
					<div id="vk_groups"></div>
					<script type="text/javascript">
					VK.Widgets.Group("vk_groups", {mode: 4, wide: 1, width: "700", height: "1000"}, 106844509);
					</script>
					
					</div>
				
				</center>
			</div>
			
			
			
			
			<div  id = "tmpl_3"	class = "blank" >   						<!--блок о нас-->
				<center>
				
				<div class = "header_block">
					О НАС
				</div>
		
				<input type = "hidden" id = "define_load_diploms" value = "0">
				
					<div id = "load_diploms" style = "width: 95%; padding-top: 20px; padding-bottom: 20px;">						
					</div>
					
					<div id = "on_of_load_diploms">
						<a onmouseover = "this.style.cursor = 'pointer'" onclick = "load_diploms()">
							<img src = "../images/deploy.png" style = "height: 40px;" >
						</a>
					</div>

				</center>
			</div>
			
			
			
			
			<div  id = "where_to_buy"	class = "blank" >   						<!--блок Где купить-->
				<center>
				
				<div class = "header_block">
					ГДЕ КУПИТЬ
				</div>
				
					<div id = "" style = "width: 95%; padding-top: 20px; padding-bottom: 20px;">
						<table style = "width: 100%;">
							<tr>
								<td style="position: relative; vertical-align: top; width: 25%;">
									<img style="position: relative; left: 10%; top: 10%; width: 60%; opacity: 1;"  src="../images/trading_network/krasnoe_beloe.png">
								</td>
								<td style="position: relative; vertical-align: top; width: 25%;">
									<img style="position: relative; left: 10%; top: 10%; width: 60%; opacity: 1;"  src="../images/trading_network/lenta.png">
								</td>
								<td style="position: relative; vertical-align: top; width: 25%;">
									<img style="position: relative; left: 10%; top: 10%; width: 60%; opacity: 1;"  src="../images/trading_network/diksy.png">
								</td>
								<td style="position: relative; vertical-align: top; width: 25%;">
									<img style="position: relative; left: 10%; top: 10%; width: 60%; opacity: 1;"  src="../images/trading_network/karusel.png">
								</td>
							</tr>
							<tr>
								<td style="position: relative; vertical-align: top; width: 25%;">
									<img style="position: relative; left: 10%; top: 10%; width: 60%; opacity: 1;"  src="../images/trading_network/5ka.png">
								</td>
								<td style="position: relative; vertical-align: top; width: 25%;">
									<img style="position: relative; left: 10%; top: 10%; width: 60%; opacity: 1;"  src="../images/trading_network/perekrestok.png">
								</td>
								<td style="position: relative; vertical-align: top; width: 25%;">
									<img style="position: relative; left: 10%; top: 10%; width: 60%; opacity: 1;"  src="../images/trading_network/monetka.png">
								</td>
								<td style="position: relative; vertical-align: top; width: 25%;">
									<img style="position: relative; left: 10%; top: 10%; width: 60%; opacity: 1;"  src="../images/trading_network/spar.png">
								</td>																
							</tr>
							<tr>
								<td style="position: relative; vertical-align: top; width: 25%;">
									<img style="position: relative; left: 10%; top: 10%; width: 60%; opacity: 1;"  src="../images/trading_network/magnit.png">
								</td>
								<td style="position: relative; vertical-align: top; width: 25%;">
									<img style="position: relative; left: 10%; top: 10%; width: 60%; opacity: 1;"  src="../images/trading_network/svetofor.png">
								</td>
								<td style="position: relative; vertical-align: top; width: 25%;">
									<img style="position: relative; left: 10%; top: 10%; width: 60%; opacity: 1;"  src="../images/trading_network/ariant.png">
								</td>
							</tr>
						</table>
					</div>
					

				</center>
			</div>
			
			
			
			
			<div  id = "kontakty"	class = "blank" >   						<!--блок о контакты-->
			<center>
				<div class = "header_block">
					КОНТАКТЫ
				</div>
			</center>
				<br>
				<div style = "position: absolute; top: 170px; left: 5%; z-index: 7; background: rgba(255, 255, 255, 0.8); padding: 20px;">

					Адрес: 454053, г. Челябинск, Троицкий тракт 52-в <br>
					+7 (351) 729-90-05 (многоканальный) <br>
					E-mail: otvet@ruslada.ru 						<br>
																	<br>
					Контакты службы контроля качества:	+7 (351) 216-15-15 				<br>
					E-mail: otvet@ruslada.ru						<br><br>
					
					● Фирменный магазин "Руслада" (Проспект Победы, д. 390) 									<br>
					● Потребительский рынок (боксы № 16 и № 18) адрес: Троицкий тракт, 15 						<br>
					● Рынок "Уральский привоз" (боксы № 173; № 183; ангар №208) адрес: Троицкий тракт, 52-а 	<br>
					● Рынок "Северный двор" (терминал, боксы №5 юг, №18 север) адрес: Свердловский тракт, 10	<br>
					● ТК "Орбита" (боксы № 135; и №41) адрес: Свердловский тракт, 8								<br>
				
				</div>
				<br>
				<div style = "width: 100%; padding-top: 20px; padding-bottom: 20px;">
					<script type="text/javascript" 
						charset="utf-8" async 
						src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=hFVEGTc90GVsUg9y0Z94SUZQtpLczvvV&amp;width=100%&amp;height=700&amp;lang=ru_RU&amp;sourceType=constructor&amp;scroll=true"></script>
				
				</div>
			</div>
			
			
			<br><br><br><br><br><br><br><br><br><br><br><br>

			
			


		</div>	
			
		

		<div style = "display: none;">
			<!-- Rating@Mail.ru counter --> 
			<script type="text/javascript">
			var _tmr = window._tmr || (window._tmr = []);
			_tmr.push({id: "3021279", type: "pageView", start: (new Date()).getTime()});
			(function (d, w, id) {
			  if (d.getElementById(id)) return;
			  var ts = d.createElement("script"); ts.type = "text/javascript"; ts.async = true; ts.id = id;
			  ts.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//top-fwz1.mail.ru/js/code.js";
			  var f = function () {var s = d.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ts, s);};
			  if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); }
			})(document, window, "topmailru-code");
			</script><noscript><div style = "position: relative; display: none;" >
			<img src="//top-fwz1.mail.ru/counter?id=3021279;js=na" style="border:0;position:absolute;left:-9999px;" alt="" />
			</div></noscript>
			<!-- //Rating@Mail.ru counter -->




			<!-- Yandex.Metrika informer --> <a href="https://metrika.yandex.ru/stat/?id=26502648&amp;from=informer" 
			target="_blank" rel="nofollow">
			<img src="https://metrika-informer.com/informer/26502648/1_0_FFFFF5FF_FFEFD5FF_0_pageviews" 
			style="width:80px; height:15px; border:0;" alt="Яндекс.Метрика" 
			title="Яндекс.Метрика: данные за сегодня (просмотры)" class="ym-advanced-informer" data-cid="26502648" data-lang="ru" />
			</a> <!-- /Yandex.Metrika informer --> <!-- Yandex.Metrika counter --> 
			<script type="text/javascript" > (function (d, w, c) 
				{ (w[c] = w[c] || []).push(function() { 
					try { w.yaCounter26502648 = new Ya.Metrika({ 	id:26502648, 
																	clickmap:true, 
																	trackLinks:true, 
																	accurateTrackBounce:true, 
																	webvisor:true, 
																	trackHash:true, 
																	ecommerce:"dataLayer" }); 
																} catch(e) { } }); 
																var n = d.getElementsByTagName("script")[0], 
																s = d.createElement("script"), 
																f = function () { n.parentNode.insertBefore(s, n); }; 
																s.type = "text/javascript"; s.async = true; 
																s.src = "https://cdn.jsdelivr.net/npm/yandex-metrica-watch/watch.js"; 
																if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); 
																</script> <noscript><div><img src="https://mc.yandex.ru/watch/26502648" style="position:absolute; left:-9999px;" alt="" />
																</div></noscript> <!-- /Yandex.Metrika counter -->
			
			
			
			<!-- Yandex.Metrika informer -->
			<a href="https://metrika.yandex.ru/stat/?id=47509066&amp;from=informer"
			target="_blank" rel="nofollow"><img src="https://informer.yandex.ru/informer/47509066/3_1_FFFFFFFF_EFEFEFFF_0_pageviews"
			style="width:88px; height:31px; border:0;" alt="Яндекс.Метрика" title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)" class="ym-advanced-informer" data-cid="47509066" data-lang="ru" /></a>
			<!-- /Yandex.Metrika informer -->

			<!-- Yandex.Metrika counter -->
			<script type="text/javascript" >
				(function (d, w, c) {
					(w[c] = w[c] || []).push(function() {
						try {
							w.yaCounter47509066 = new Ya.Metrika2({
								id:47509066,
								clickmap:true,
								trackLinks:true,
								accurateTrackBounce:true,
								webvisor:true,
								trackHash:true
							});
						} catch(e) { }
					});

					var n = d.getElementsByTagName("script")[0],
						s = d.createElement("script"),
						f = function () { n.parentNode.insertBefore(s, n); };
					s.type = "text/javascript";
					s.async = true;
					s.src = "https://mc.yandex.ru/metrika/tag.js";

					if (w.opera == "[object Opera]") {
						d.addEventListener("DOMContentLoaded", f, false);
					} else { f(); }
				})(document, window, "yandex_metrika_callbacks2");
			</script>
			<noscript><div><img src="https://mc.yandex.ru/watch/47509066" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
			<!-- /Yandex.Metrika counter -->


			<!-- BEGIN JIVOSITE CODE {literal} -->
			<script type='text/javascript'>
			(function(){ var widget_id = 'EflVVg0sd6';var d=document;var w=window;function l(){
			var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>
			<!-- {/literal} END JIVOSITE CODE -->	


			<!-- Rating@Mail.ru logo -->
			<a href="https://top.mail.ru/jump?from=3021279" style = "position: relative; z-index: 0;">
			<img src="//top-fwz1.mail.ru/counter?id=3021279;t=479;l=1" 
			style="border:0;" height="31" width="88" alt="Рейтинг@Mail.ru" /></a>
			<!-- //Rating@Mail.ru logo -->
		
		</div>
	

</body>

</html>


