

window.onscroll = function()
	{
		var coord = window.pageYOffset;															/* определяем текущее положение скролла на странице */
		document.getElementById('coordinate').value = coord;									/* пишем значение положения скролла в буфер координаты */
		
		
		document.getElementById("goto_vk").style.top = coord + 20 + "px";						// двигаем ярлык vk
		document.getElementById("tel").style.top = coord + 20 + "px";			// двигаем обратный звонок
			
		
		// процедура паралакса основной заставки
		
		var screen_width = document.body.clientWidth;   										// определяем текущую ширину окна браузера
		
		
		
		if (coord > screen_width/(1900/1350))													// замена z-index на логотипе и поисковике
			{
				document.getElementById("roof").style.zIndex = "100";
			}
		else
			{
				document.getElementById("roof").style.zIndex = "0";
			}
			
			
				
		var divider_cloud_00 = screen_width/(1900/29);											// делители значения скролла для каждого слоя
		var divider_logo = screen_width/(1900/20);
		
		var divider_pastry_01 = screen_width/(1900/15);
		var divider_pastry_02 = screen_width/(1900/19);

		var divider_cloud_01 = screen_width/(1900/45);
		var divider_cloud_02 = screen_width/(1900/100);
		var divider_cloud_03 = screen_width/(1900/30);
		var divider_cloud_05 = screen_width/(1900/60);
		var divider_cloud_06 = screen_width/(1900/100);
		var divider_cloud_04 = screen_width/(1900/50);
		var divider_cloud_06_2 = screen_width/(1900/45);
						
		
		if (coord < screen_width/19*150)														// задний фон - небо: видимый / скрытый
			{
				document.getElementById('cloud_00').style.display = "block";
			}
		else
			{
				document.getElementById('cloud_00').style.display = "none";
			} 
			
		if (coord < screen_width/19*9)
			{
				document.getElementById('cloud_00').style.top = -15 + coord/divider_cloud_00 + '%';
			}

			
		if (coord < screen_width/19*7)
			{
				document.getElementById('logo').style.top = 5 + coord/divider_logo + '%';
				document.getElementById('logo').style.display = "block";
			}
		else
			{
				document.getElementById('logo').style.display = "none";
			}
			
			

			
		if (coord < screen_width/19*28)
			{
				document.getElementById('pastry_01').style.top = 0 + coord/divider_pastry_01 + '%';
				document.getElementById('pastry_01').style.width = 100 - coord/divider_pastry_01/4 + '%';
			}
			
		if (coord < screen_width/19*5)
			{
				document.getElementById('pastry_02').style.top = 0 + coord/divider_pastry_02 + '%';
				document.getElementById('pastry_02').style.width = 100 - coord/divider_pastry_01/10 + '%';
			}
		else
			{
				document.getElementById('pastry_02').style.top = -3 + coord/divider_pastry_02*1.1 + '%';
				document.getElementById('pastry_02').style.width = 100 - coord/divider_pastry_01/10 + '%';
			}
			
		/* if (coord < screen_width/19*23)
			{
				document.getElementById('cloud_01').style.display = "block";
			}
		else
			{
				document.getElementById('cloud_01').style.display = "none";
			} */
			
	if (coord < screen_width/19*3)
			{
				document.getElementById('cloud_01').style.top = 5 + coord/divider_cloud_01 + '%';
			}

		document.getElementById('cloud_02').style.top = 20 - coord/divider_cloud_02 + '%';
				
		document.getElementById('cloud_05').style.top = 30 - coord/divider_cloud_05 + '%';


			
		document.getElementById('cloud_03').style.top = 21 - coord/divider_cloud_03 + '%';
		
		document.getElementById('cloud_06').style.top = 20 - coord/divider_cloud_06 + '%';
		
		document.getElementById('cloud_04').style.top = 30 - coord/divider_cloud_04 + '%';
		
		
	/*	if (coord < screen_width/1*0.1)
			{
				document.getElementById('cloud_06_2').style.display = "none";
			}
		else
			{
				document.getElementById('cloud_06_2').style.display = "block";
			} 
			
			
			
		if (coord < screen_width/19*17.8)
			{
				document.getElementById('cloud_06_2').style.top = 40 + coord/divider_cloud_06_2 + '%';
			}
			
			
		if (coord < screen_width*1.5)
			{
				document.getElementById('roof').style.display = "none";
			}
		else 
			{
				document.getElementById('roof').style.display = "block";
			} */
		
		
		// закрываем процедуру
		
		
		
		
		
		var id_tmpl_1 = document.getElementById("tmpl_1");     							/* обращаемся к первому блоку */
		id_tmpl_1 = id_tmpl_1.offsetTop;												/* определяем положение блока по вертикали */

		var id_tmpl_2 = document.getElementById("tmpl_2");     							/* обращаемся ко второму блоку */
		id_tmpl_2 = id_tmpl_2.offsetTop;												/* определяем положение блока по вертикали */
		
		var id_tmpl_news = document.getElementById("tmpl_news");     					/* обращаемся к блоку новостей */
		id_tmpl_news = id_tmpl_news.offsetTop;											/* определяем положение блока по вертикали */
		
		var id_tmpl_3 = document.getElementById("tmpl_3");     							/* обращаемся к третьему блоку */
		id_tmpl_3 = id_tmpl_3.offsetTop;												/* определяем положение блока по вертикали */
		
		var id_where_to_buy = document.getElementById("where_to_buy");     				/* обращаемся к блоку контакты */
		id_where_to_buy = where_to_buy.offsetTop;										/* определяем положение блока по вертикали */
		
		var id_kontakty = document.getElementById("kontakty");     						/* обращаемся к блоку контакты */
		id_kontakty = kontakty.offsetTop;												/* определяем положение блока по вертикали */
		
		
		
		
		if (coord > id_tmpl_2-30 && coord < id_tmpl_1-30)									/* если  положение скролла больше положения первого блока, но меньше положения второго ... */
			{
				document.getElementById("current_tmpl").value = "tmpl_2";  					/* пишем имя текущего шаблона в буфер current_tmpl */
				var buf_section1 = document.getElementById("section1").value;						/* считываем значение раздела первого порядка из буфера */
				if (buf_section1 == "tmpl_2")
					{
						document.getElementById("punkt_tmpl_2").style.background = "#A92725";
						document.getElementById("punkt_tmpl_2_all_creators").style.background = "none";
					}
				else
					{
						if (buf_section1 == "tmpl_2___!!!___all_creators")							/* условие для закрашивания пункта - "производители" или пункта "каталог" */
							{
								document.getElementById("punkt_tmpl_2").style.background = "none";
								document.getElementById("punkt_tmpl_2_all_creators").style.background = "#A92725";
							}
						else
							{
								document.getElementById("punkt_tmpl_2").style.background = "#A92725";
								document.getElementById("punkt_tmpl_2_all_creators").style.background = "none";
							}
					}
				
			}
		else
			{
				document.getElementById("punkt_tmpl_2").style.background = "none";
				document.getElementById("punkt_tmpl_2_all_creators").style.background = "none";
			}
			
		if (coord > id_tmpl_1-30 && coord < id_tmpl_news-30)								/* если  положение скролла больше положения второго блока, но меньше положения третьего ... */
			{
				document.getElementById("current_tmpl").value = "tmpl_1";  					/* пишем имя текущего шаблона в буфер current_tmpl */
				document.getElementById("punkt_tmpl_1").style.background = "#A92725";
			}
		else
			{
				document.getElementById("punkt_tmpl_1").style.background = "none";
			}
			
		if (coord > id_tmpl_news-30 && coord < id_tmpl_3-30)								/* если  положение скролла больше положения блока новостей, но меньше положения третьего ... */
			{
				document.getElementById("current_tmpl").value = "tmpl_news";  				/* пишем имя текущего шаблона в буфер current_tmpl */
				document.getElementById("punkt_news").style.background = "#A92725";
			}
		else
			{
				document.getElementById("punkt_news").style.background = "none";
			}
		
			
		if (coord > id_tmpl_3-30 && coord < id_where_to_buy-30)									/* если  положение скролла больше положения третьего блока ... */
			{
				document.getElementById("current_tmpl").value = "tmpl_3";  					/* пишем имя текущего шаблона в буфер current_tmpl */
				document.getElementById("punkt_tmpl_3").style.background = "#A92725";
			}
		else
			{
				document.getElementById("punkt_tmpl_3").style.background = "none";
			}
			
		if (coord > id_where_to_buy-30 && coord < id_kontakty-30)									/* если  положение скролла больше положения третьего блока ... */
			{
				document.getElementById("current_tmpl").value = "where_to_buy";  					/* пишем имя текущего шаблона в буфер current_tmpl */
				document.getElementById("punkt_where_to_buy").style.background = "#A92725";
			}
		else
			{
				document.getElementById("punkt_where_to_buy").style.background = "none";
			}
			
		if (coord > id_kontakty-30)															/* если  положение скролла больше блока контакты ... */
			{
				document.getElementById("current_tmpl").value = "kontakty";  					/* пишем имя текущего шаблона в буфер current_tmpl */
				document.getElementById("punkt_kontakty").style.background = "#A92725";
			}
		else
			{
				document.getElementById("punkt_kontakty").style.background = "none";
			}
	}
	
	
	
	
	
	
	
	
	function control_url()
		{
			/* основная процедура обработки url  -  подумать над коррекцией еще !!!!   */
			
				var current_url = document.location.href;										/* считываем и обрабатываем текущий URL*/				
				current_url = current_url.split('#');											/* разбиваем адрес на массив подразделов */
				
				if (current_url[1] != undefined)												/* если часть url после знака # не пустое ... */
					{
						current_url = current_url[1].split('/');										/* разбиваем адрес на массив подразделов */
						
						section1 = current_url[0];														/* определяем раздел первого порядка */
						section2 = current_url[1];														/* определяем раздел второго порядка */
						
						if (section2 != undefined)														/* определяем тип продукции */
							{
								var type_product = section2.split("___!!!___");
								type_product = type_product[1];
							}
							
						section3 = current_url[2];														/* определяем раздел третьего порядка */
						section4 = current_url[3];														/* определяем раздел четвертого порядка ( номер страницы ) */
						section5 = current_url[4];														/* определяем раздел пятого порядка */
					}
				else
					{
						section1 = undefined;
						section2 = undefined;
						section3 = undefined;
						section4 = undefined;
						section5 = undefined;
					}
					
					
																						
			/* закрываем процедуру */
			
			
			
			var buf_section1 = document.getElementById("section1").value;						/* считываем значение раздела первого порядка из буфера */
			var buf_section2 = document.getElementById("section2").value;						/* считываем значение раздела второго порядка из буфера */
			
			var buf_type_product = document.getElementById("type_product").value;				/* считываем тип продукции из буфера */
			
			var buf_section3 = document.getElementById("section3").value;						/* считываем значение раздела третьего порядка из буфера */
			var buf_section4 = document.getElementById("section4").value;						/* считываем значение раздела четвертого порядка из буфера ( номер страницы ) */
			var buf_section5 = document.getElementById("section5").value;						/* считываем значение раздела пятого порядка из буфера */
			
			
						
			
		/* процедура переключения страниц продукции */
				
			if (section4 != undefined)															/* если содержимое url четвертого порядка не пустое ...*/													
				{
					if (buf_section4 != section4)
						{
							unlock();
							document.getElementById("section4").value = section4;				/* печатаем номер страницы в буфер section4*/
						}
				}
			else
				{
					document.getElementById("section4").value = section4;						/* печатаем номер страницы в буфер section4*/
				}
				
				
				
				
				
				
				
		/* процедура переключения типов продукции */
				
			if (type_product != '')																/* если содержимое url тип пролукции не пустое ...*/
				{
					if (type_product == undefined)
						{
							type_product = '';
						}
					if (buf_type_product != type_product)
						{
							unlock();
							document.getElementById("type_product").value = type_product;		/* пишем тип продукции в буфер */
						}
				}
			else
				{
					document.getElementById("type_product").value = type_product;		/* пишем тип продукции в буфер */
				}
				
				
				
				
				


						
			if (section1 != undefined)															/* если содержимое url после знака # не пустое ...*/
				{
					section1 = section1.substr(5);												/* убираем приставку link_ */
					
					if (section1 != buf_section1)												/* если значения раздела первого порядка не совпадает с буфером имени первого раздела ... */
						{	
							if (section1 != buf_section1)		/* функция для селекции разделы продукции или все производители на главной страницы каталога */
							{
								unlock();
							}
							document.getElementById("section1").value = section1;				/* пишем имя раздела первого порядка в буфер */
							document.getElementById("section2").value = section2;				/* пишем имя раздела второго порядка в буфер */
							
							document.getElementById("type_product").value = type_product;		/* пишем тип продукции в буфер */
							
							document.getElementById("section3").value = section3;				/* пишем имя раздела третьего порядка в буфер */
							document.getElementById("section5").value = section5;				/* пишем имя раздела пятого порядка в буфер */
							move_scroll();														/* включаем двигатель */
						}
					else
						{
							document.getElementById("section1").value = section1;				/* пишем имя раздела первого порядка в буфер */
							document.getElementById("section2").value = section2;				/* пишем имя раздела второго порядка в буфер */
							document.getElementById("section3").value = section3;				/* пишем имя раздела третьего порядка в буфер */
							document.getElementById("section5").value = section5;				/* пишем имя раздела пятого порядка в буфер */
						}
						
			
			
						
					if (section1 == "tmpl_1")                    // условие для развертки журналов
						{
							if (buf_section1 != "tmpl_1")		// процедура загрузки журналов
								{							
									unlock();
									load_journal_action();
									unload_diploms();
									goto_confectionery();																	// выводим вид продукции "кондитерка"
									document.getElementById("define_load_journal_action").value = 0;
								}
						}                                                         
					else
						{
																// процедура отгрузки журналов
																
							var define_load_journal_action = document.getElementById("define_load_journal_action").value;
							
							if (define_load_journal_action != 1)
								{
									unload_journal_action();
									document.getElementById("define_load_journal_action").value = 1;
								}
						}
						

						
						
					
					if (section1 == "conservation")					// выводим вид продукции "консервация"
						{
							document.getElementById('select_kind_of_products').innerHTML = 'Консервация<br>и бакалея'; 
							if (buf_section1 != "conservation")
								{
									unload_diploms();
									unload_journal_action();
									goto_conservation();
								}
						}
						
						
					if (section1 == "new_year_products")					// выводим вид продукции "консервация"
						{
							document.getElementById('select_kind_of_products').innerHTML = 'Новогодняя<br>продукция'; 
							if (buf_section1 != "new_year_products")
								{
									unload_diploms();
									unload_journal_action();
									goto_new_year_products();
								}
						}
						
						
						
						
						
					if (section1 == "where_to_buy")					// движемся к разделу "где купить"
						{
							if (buf_section1 != "where_to_buy")
								{
									unload_diploms();
									unload_journal_action();
									goto_confectionery();
								}
						}
						
						
					if (section1 == "kontakty")					// движемся к разделу "контакты"
						{
							if (buf_section1 != "kontakty")
								{
									unload_diploms();
									unload_journal_action();
									goto_confectionery();
								}
						}
						
																	
						
						
					if (section1 == "tmpl_news")                   	// условие для развертки новостей
						{
							if (buf_section1 != "tmpl_news")			// процедура загрузки новостей
								{							
									unlock();
									unload_journal_action();
									unload_diploms();
									goto_confectionery();																	// выводим вид продукции "кондитерка"
									document.getElementById("define_load_news").value = 0;
								}
						}                                                         
					else
						{
																	// процедура отгрузки новостей
																
							var define_load_journal_action = document.getElementById("define_load_news").value;
							
							if (define_load_news != 1)
								{
									document.getElementById("define_load_news").value = 1;
								}
						}
					
					
					
					
					if (section1 == "tmpl_3")                    	// условие для развертки дипломов
						{
							if (buf_section1 != "tmpl_3")			// процедура загрузки дипломов
								{							
									unlock();
									load_diploms();
									unload_journal_action();
									goto_confectionery();																	// выводим вид продукции "кондитерка"
									document.getElementById("define_load_diploms").value = 0;
								}
						}                                                         
					else
						{
																	// процедура отгрузки дипломов
																
							var define_load_diploms = document.getElementById("define_load_diploms").value;
							
							if (define_load_diploms != 1)
								{
									unload_diploms();
									document.getElementById("define_load_diploms").value = 1;
								}
						}
							

							
				}
			else
				{
					document.location.hash = '#link_tmpl_0';
					document.getElementById('section1').value = "tmpl_0";
					var coord = window.pageYOffset;
					if (coord > 1000)
						{
							move_scroll();
						}
				}	
				
				
				
				
				
				
			if (section2 != undefined)															/* если содержимое url второго порядка не пустое ...*/
				{

					slovo = section2;
					
					/* скрипт расшифратор на русский*/
					

					var rus_alphabet = "/ /й/ц/у/к/е/ё/н/г/ш/щ/з/х/ъ/ф/ы/в/а/п/р/о/л/д/ж/э/я/ч/с/м/и/т/ь/б/ю/q/w/e/r/t/y/u/i/o/p/a/s/d/f/g/h/j/k/l/z/x/c/v/b/n/m/Й/Ц/У/К/Е/Ё/Н/Г/Ш/Щ/З/Х/Ъ/Ф/Ы/В/А/П/Р/О/Л/Д/Ж/Э/Я/Ч/С/М/И/Т/Ь/Б/Ю/Q/W/E/R/T/Y/U/I/O/P/A/S/D/F/G/H/J/K/L/Z/X/C/V/B/N/M/0/1/2/3/4/5/6/7/8/9/-/,/./!/+";			/* русский-английский алфавит */
					var eng_alphabet = "/_/y/c/u/k/e/yo/n/g/sh/ss/z/h/qy/f/yy/v/a/p/r/o/l/d/j/ay/ya/ch/s/m/i/t/yi/b/yu/q_/w_/e_/r_/t_/y_/u_/i_/o_/p_/a_/s_/d_/f_/g_/h_/j_/k_/l_/z_/x_/c_/v_/b_/n_/m_/Y/C/U/K/E/YO/N/G/SH/SS/Z/H/QY/F/YY/V/A/P/R/O/L/D/J/AY/YA/CH/S/M/I/T/YI/B/YU/Q_/W_/E_/R_/T_/Y_/U_/I_/O_/P_/A_/S_/D_/F_/G_/H_/J_/K_/L_/Z_/X_/C_/V_/B_/N_/M_/0/1/2/3/4/5/6/7/8/9/--/,/./!/+";		/* псевдо-английский алфавит */

					var slovo = slovo.split('?');

					var summa = ""; 		/* сумматор */

					var l_slovo = slovo;
					l_slovo = l_slovo.length;                		/* длинна фразы */

					rus_alphabet = rus_alphabet.split("/");			/* задаем массивы соответствия через регулярное выражение */
					eng_alphabet = eng_alphabet.split("/");
					
					var l_alphabet = rus_alphabet.length;			/* длинна массива */
						
					for (i = 0; i <= l_slovo-1; i++)
						{
							for (j = 0; j <= l_alphabet; j++)
								{
									if (slovo[i] == eng_alphabet[j])
										{
											var char_bufer = rus_alphabet[j];
											summa = summa + char_bufer;			/* если значение символа во фразе присутствует заменяем его на соответствующий псевдо-символ */
										}
								}
						}
					slovo = summa;												/* возвращаем переведенную фразу */
					
					/* закрываем скрипт  */
					
					
					
				
				
				
					var name_section = slovo;
					
					
					
					if (section3 != undefined)
						{
							if (section5 != undefined)             /* если раздел пятого порядка не тривиален запускаем все процедуры для загрузки страницы выбранного товара */
								{
								
									var lock5 = document.getElementById("lock5").value;
									if (lock5 == 0)
										{
											// процедура определения вида продукции (кондитерка, консервация и т. д.)
												var kind_of_products = document.getElementById("kind_of_products").value;		
												if (kind_of_products == "confectionery")
													{
														document.getElementById('select_kind_of_products').innerHTML = 'Кондитерcкие<br> изделия';
													}
												if (kind_of_products == "conservation")
													{
														document.getElementById('select_kind_of_products').innerHTML = 'Конcервация<br>и бакалея';
													}
												if (kind_of_products == "new_year_products")
													{
														document.getElementById('select_kind_of_products').innerHTML = 'Новогодняя<br>продукция';
													}
											// закрываем процедуру
											
											unlock();
											
											// меняем класс у клавиши "КАТАЛОГ ПРОДУКЦИИ"
											document.getElementById("section_production").className = "second_header  section_creators_active";
											// меняем класс у клавиши "ПРОИЗВОДИТЕЛИ"
											document.getElementById("creators_production").className = "header_block  section_creators";
											
											document.getElementById("navigation_logo_creator").innerHTML = "<a onclick = \' read_company_active_for_user() \'> " + name_section + " /</a>";  // раздел продукции в навигационной цепочке
											
											slovo = section3;
							
											/* скрипт расшифратор на русский*/
											

											var rus_alphabet = "/ /й/ц/у/к/е/ё/н/г/ш/щ/з/х/ъ/ф/ы/в/а/п/р/о/л/д/ж/э/я/ч/с/м/и/т/ь/б/ю/q/w/e/r/t/y/u/i/o/p/a/s/d/f/g/h/j/k/l/z/x/c/v/b/n/m/Й/Ц/У/К/Е/Ё/Н/Г/Ш/Щ/З/Х/Ъ/Ф/Ы/В/А/П/Р/О/Л/Д/Ж/Э/Я/Ч/С/М/И/Т/Ь/Б/Ю/Q/W/E/R/T/Y/U/I/O/P/A/S/D/F/G/H/J/K/L/Z/X/C/V/B/N/M/0/1/2/3/4/5/6/7/8/9/-/,/./!/+";			/* русский-английский алфавит */
											var eng_alphabet = "/_/y/c/u/k/e/yo/n/g/sh/ss/z/h/qy/f/yy/v/a/p/r/o/l/d/j/ay/ya/ch/s/m/i/t/yi/b/yu/q_/w_/e_/r_/t_/y_/u_/i_/o_/p_/a_/s_/d_/f_/g_/h_/j_/k_/l_/z_/x_/c_/v_/b_/n_/m_/Y/C/U/K/E/YO/N/G/SH/SS/Z/H/QY/F/YY/V/A/P/R/O/L/D/J/AY/YA/CH/S/M/I/T/YI/B/YU/Q_/W_/E_/R_/T_/Y_/U_/I_/O_/P_/A_/S_/D_/F_/G_/H_/J_/K_/L_/Z_/X_/C_/V_/B_/N_/M_/0/1/2/3/4/5/6/7/8/9/--/,/./!/+";		/* псевдо-английский алфавит */

											var slovo = slovo.split('?');

											var summa = ""; 		/* сумматор */

											var l_slovo = slovo;
											l_slovo = l_slovo.length;                		/* длинна фразы */

											rus_alphabet = rus_alphabet.split("/");			/* задаем массивы соответствия через регулярное выражение */
											eng_alphabet = eng_alphabet.split("/");
											
											var l_alphabet = rus_alphabet.length;			/* длинна массива */
												
											for (i = 0; i <= l_slovo-1; i++)
												{
													for (j = 0; j <= l_alphabet; j++)
														{
															if (slovo[i] == eng_alphabet[j])
																{
																	var char_bufer = rus_alphabet[j];
																	summa = summa + char_bufer;			/* если значение символа во фразе присутствует заменяем его на соответствующий псевдо-символ */
																}
														}
												}
											slovo = summa;												/* возвращаем переведенную фразу */
											
											/* закрываем скрипт  */
																				
											
											var name_company = slovo;
											
											document.getElementById("navigation_produkciya").innerHTML = '<a onclick = "read_products_for_user()">' + name_company + ' /</a>';     // имя производителя в навигационной цепочке
											
											
											slovo = section5;

											
											/* скрипт расшифратор на русский*/
											
											var rus_alphabet = "/ /й/ц/у/к/е/ё/н/г/ш/щ/з/х/ъ/ф/ы/в/а/п/р/о/л/д/ж/э/я/ч/с/м/и/т/ь/б/ю/q/w/e/r/t/y/u/i/o/p/a/s/d/f/g/h/j/k/l/z/x/c/v/b/n/m/Й/Ц/У/К/Е/Ё/Н/Г/Ш/Щ/З/Х/Ъ/Ф/Ы/В/А/П/Р/О/Л/Д/Ж/Э/Я/Ч/С/М/И/Т/Ь/Б/Ю/Q/W/E/R/T/Y/U/I/O/P/A/S/D/F/G/H/J/K/L/Z/X/C/V/B/N/M/0/1/2/3/4/5/6/7/8/9/-/,/./!/___plus___";			/* русский-английский алфавит */
											var eng_alphabet = "/_/y/c/u/k/e/yo/n/g/sh/ss/z/h/qy/f/yy/v/a/p/r/o/l/d/j/ay/ya/ch/s/m/i/t/yi/b/yu/q_/w_/e_/r_/t_/y_/u_/i_/o_/p_/a_/s_/d_/f_/g_/h_/j_/k_/l_/z_/x_/c_/v_/b_/n_/m_/Y/C/U/K/E/YO/N/G/SH/SS/Z/H/QY/F/YY/V/A/P/R/O/L/D/J/AY/YA/CH/S/M/I/T/YI/B/YU/Q_/W_/E_/R_/T_/Y_/U_/I_/O_/P_/A_/S_/D_/F_/G_/H_/J_/K_/L_/Z_/X_/C_/V_/B_/N_/M_/0/1/2/3/4/5/6/7/8/9/--/,/./!/+";		/* псевдо-английский алфавит */

											var slovo = slovo.split('?');

											var summa = ""; 		/* сумматор */

											var l_slovo = slovo;
											l_slovo = l_slovo.length;                		/* длинна фразы */

											rus_alphabet = rus_alphabet.split("/");			/* задаем массивы соответствия через регулярное выражение */
											eng_alphabet = eng_alphabet.split("/");
											
											var l_alphabet = rus_alphabet.length;			/* длинна массива */
												
											for (i = 0; i <= l_slovo-1; i++)
												{
													for (j = 0; j <= l_alphabet; j++)
														{
															if (slovo[i] == eng_alphabet[j])
																{
																	var char_bufer = rus_alphabet[j];
																	summa = summa + char_bufer;			/* если значение символа во фразе присутствует заменяем его на соответствующий псевдо-символ */
																}
														}
												}
											slovo = summa;												/* возвращаем переведенную фразу */
											var name_tovar = slovo;
											
											/*  закрываем скрипт  */
											
								
											$("#catalog").load("../config/read_page_product.php", "name_tovar="+name_tovar);
											
											var commas = name_tovar.split("...");										/* определяем количество замененных кавычек */
											commas = commas.length;
											
											for (i = 0; i <= commas+2; i++)
												{
													name_tovar = name_tovar.replace("...", "&quot");			/* возвращаем кавычки */
												}
											
											document.getElementById("page_produkciya").innerHTML = "<a>" + name_tovar + "</a>";						// наименование товара в навигационной цепочке
											document.getElementById("lock5").value = 1;                                                          		// закрываем замок 5
											move_scroll();
										}										
																				
								}
							else									/* если раздел пятого порядка тривиален запускаем все процедуры для загрузки страницы товаров выбранного производителя 3-4 порядок */
								{ 
									// процедура определения вида продукции (кондитерка, консервация и т. д.)
									var kind_of_products = document.getElementById("kind_of_products").value;		
									if (kind_of_products == "confectionery")
										{
											document.getElementById('select_kind_of_products').innerHTML = 'Кондитерcкие<br> изделия';
										}
									if (kind_of_products == "conservation")
										{
											document.getElementById('select_kind_of_products').innerHTML = 'Конcервация<br>и бакалея';
										}
									if (kind_of_products == "new_year_products")
										{
											document.getElementById('select_kind_of_products').innerHTML = 'Новогодняя<br>продукция';
										}
									// закрываем процедуру
									
									var lock3 = document.getElementById("lock3").value;
									if (lock3 == 0)
										{	
											unlock();
											slovo = section3;
							
											/* скрипт расшифратор на русский*/
											

											var rus_alphabet = "/ /й/ц/у/к/е/ё/н/г/ш/щ/з/х/ъ/ф/ы/в/а/п/р/о/л/д/ж/э/я/ч/с/м/и/т/ь/б/ю/q/w/e/r/t/y/u/i/o/p/a/s/d/f/g/h/j/k/l/z/x/c/v/b/n/m/Й/Ц/У/К/Е/Ё/Н/Г/Ш/Щ/З/Х/Ъ/Ф/Ы/В/А/П/Р/О/Л/Д/Ж/Э/Я/Ч/С/М/И/Т/Ь/Б/Ю/Q/W/E/R/T/Y/U/I/O/P/A/S/D/F/G/H/J/K/L/Z/X/C/V/B/N/M/0/1/2/3/4/5/6/7/8/9/-/,/./!/+";			/* русский-английский алфавит */
											var eng_alphabet = "/_/y/c/u/k/e/yo/n/g/sh/ss/z/h/qy/f/yy/v/a/p/r/o/l/d/j/ay/ya/ch/s/m/i/t/yi/b/yu/q_/w_/e_/r_/t_/y_/u_/i_/o_/p_/a_/s_/d_/f_/g_/h_/j_/k_/l_/z_/x_/c_/v_/b_/n_/m_/Y/C/U/K/E/YO/N/G/SH/SS/Z/H/QY/F/YY/V/A/P/R/O/L/D/J/AY/YA/CH/S/M/I/T/YI/B/YU/Q_/W_/E_/R_/T_/Y_/U_/I_/O_/P_/A_/S_/D_/F_/G_/H_/J_/K_/L_/Z_/X_/C_/V_/B_/N_/M_/0/1/2/3/4/5/6/7/8/9/--/,/./!/+";		/* псевдо-английский алфавит */

											var slovo = slovo.split('?');

											var summa = ""; 		/* сумматор */

											var l_slovo = slovo;
											l_slovo = l_slovo.length;                		/* длинна фразы */

											rus_alphabet = rus_alphabet.split("/");			/* задаем массивы соответствия через регулярное выражение */
											eng_alphabet = eng_alphabet.split("/");
											
											var l_alphabet = rus_alphabet.length;			/* длинна массива */
												
											for (i = 0; i <= l_slovo-1; i++)
												{
													for (j = 0; j <= l_alphabet; j++)
														{
															if (slovo[i] == eng_alphabet[j])
																{
																	var char_bufer = rus_alphabet[j];
																	summa = summa + char_bufer;			/* если значение символа во фразе присутствует заменяем его на соответствующий псевдо-символ */
																}
														}
												}
											slovo = summa;												/* возвращаем переведенную фразу */
											
											/* закрываем скрипт  */
																				
											
											var name_company = slovo;									
											var npage = section4;
											
											$("#catalog").load("../config/read_base.php", "name_company="+name_company+"&name_product="+name_section+"&npage="+npage+"&type_product="+type_product);
											
											setTimeout("page_select_class()", 100);   // меняем класс выбранной цыфры на селекторе страниц
											setTimeout("page_select_class()", 200);   // меняем класс выбранной цыфры на селекторе страниц
											setTimeout("page_select_class()", 300);   // меняем класс выбранной цыфры на селекторе страниц
											setTimeout("page_select_class()", 400);   // меняем класс выбранной цыфры на селекторе страниц
											setTimeout("page_select_class()", 500);   // меняем класс выбранной цыфры на селекторе страниц
											
											document.getElementById("navigation_logo_creator").innerHTML = "<a onclick = \' read_company_active_for_user() \'> " + name_section + " /</a>";
											document.getElementById("navigation_produkciya").innerHTML = "<a>" + name_company + " /</a>";
											document.getElementById("page_produkciya").innerHTML = "<a>Страница " + npage + "</a>";
											
											// меняем корень в хлебных крошках
											document.getElementById("root").innerHTML = "<a class = 'font_days' onmouseover = \"this.style.cursor = 'pointer'\" onclick = 'back_general_catalog()'>каталог /</a>";
											
											
											// меняем класс у клавиши "КАТАЛОГ ПРОДУКЦИИ"
											document.getElementById("section_production").className = "second_header  section_creators_active";
											// меняем класс у клавиши "ПРОИЗВОДИТЕЛИ"
											document.getElementById("creators_production").className = "header_block  section_creators";
											
											
											document.getElementById("lock1").value = 0;																// открываем замок 1
											document.getElementById("lock2").value = 0;																// открываем замок 2
											document.getElementById("lock3").value = 1;                                                          	// закрываем замок 3
											
											move_scroll();
										}
									else
										{
											if (section2 != buf_section2) 		// условие необходимо для навигации по разделам продукции у данного производителя в menu_section
											{
												unlock();
											}
										}
								} 
						}
					else
						{
							// процедура определения вида продукции (кондитерка, консервация и т. д.)
									var kind_of_products = document.getElementById("kind_of_products").value;		
									if (kind_of_products == "confectionery")
										{
											document.getElementById('select_kind_of_products').innerHTML = 'Кондитерcкие<br> изделия';
										}
									if (kind_of_products == "conservation")
										{
											document.getElementById('select_kind_of_products').innerHTML = 'Конcервация<br>и бакалея';
										}
									if (kind_of_products == "new_year_products")
										{
											document.getElementById('select_kind_of_products').innerHTML = 'Новогодняя<br>продукция';
										}
							// закрываем процедуру
							
							var lock2 = document.getElementById("lock2").value;
							if (lock2 == 0)
								{
									// меняем класс у клавиши "КАТАЛОГ ПРОДУКЦИИ"
									document.getElementById("section_production").className = "second_header  section_creators_active";
									// меняем класс у клавиши "ПРОИЗВОДИТЕЛИ"
									document.getElementById("creators_production").className = "header_block  section_creators";
										
									// меняем корень в хлебных крошках
									document.getElementById("root").innerHTML = "<a class = 'font_days' onmouseover = \"this.style.cursor = 'pointer'\" onclick = 'back_general_catalog()'>каталог /</a>";		
									
									$("#catalog").load("../config/read_company_active_for_user.php", "name_section="+name_section+"&type_product="+type_product);         // вызываем процедуру считывания активных производителей
									document.getElementById("navigation_logo_creator").innerHTML = "<a onclick = \' read_company_active_for_user() \'> " + name_section + " /</a>";
									document.getElementById("navigation_produkciya").innerHTML = "";
									document.getElementById("page_produkciya").innerHTML = "";
									document.getElementById("lock1").value = 0;																// открываем замок 1
									document.getElementById("lock2").value = 1;																// закрываем замок 2
									document.getElementById("lock3").value = 0;                                                          	// открываем замок 3
									move_scroll();
								}
						}						
				}
			else 
				{
					var lock1 = document.getElementById("lock1").value;
					if (lock1 == 0)
						{
							if (section1 == "tmpl_2")
								{
									// процедура определения вида продукции (кондитерка, консервация и т. д.)
									var kind_of_products = document.getElementById("kind_of_products").value;		
									if (kind_of_products == "confectionery")
										{
											document.getElementById('select_kind_of_products').innerHTML = 'Кондитерcкие<br> изделия';
										}
									if (kind_of_products == "conservation")
										{
											document.getElementById('select_kind_of_products').innerHTML = 'Конcервация<br>и бакалея';
										}
									if (kind_of_products == "new_year_products")
										{
											document.getElementById('select_kind_of_products').innerHTML = 'Новогодняя<br>продукция';
										}
									// закрываем процедуру
									
									goto_confectionery();														// выводим вид продукции "кондитерка"
									
									load_general_catalog();														// загружаем генеральную страницу каталога с разделами продукции файл: read_section_products_for_user.php	
									document.getElementById("navigation_logo_creator").innerHTML = "";
									document.getElementById("navigation_produkciya").innerHTML = "";
									document.getElementById("page_produkciya").innerHTML = "";
									document.getElementById("lock1").value = 1;									// закрываем замок 1
									document.getElementById("lock2").value = 0;									// открываем замок 2
									document.getElementById("lock3").value = 0;                                 // открываем замок 3
									
									// меняем класс у клавиши "КАТАЛОГ ПРОДУКЦИИ"
									document.getElementById("section_production").className = "second_header  section_creators_active";
									// меняем класс у клавиши "ПРОИЗВОДИТЕЛИ"
									document.getElementById("creators_production").className = "header_block  section_creators";
									
									// меняем корень в хлебных крошках
									document.getElementById("root").innerHTML = "<a class = 'font_days' onmouseover = \"this.style.cursor = 'pointer'\" onclick = 'back_general_catalog()'>каталог /</a>";  
									
									move_scroll();
								}
							else
								{
									if (section1 == 'conservation')                                            // если #link_conservation пишем вид продукции - консервация и бакалея
										{
											document.getElementById("kind_of_products").value = "conservation";
										}
									if (section1 == 'new_year_products')                                            // если #link_new_year_products пишем вид продукции - консервация и бакалея
										{
											document.getElementById("kind_of_products").value = "new_year_products";
										}
										
									if (section1 == "tmpl_2___!!!___all_creators")
										{											
											all_creators();	
											
											// меняем корень в хлебных крошках
											document.getElementById("root").innerHTML = "<a class = 'font_days' onmouseover = \"this.style.cursor = 'pointer'\" onclick = 'look_all_creators()' >производители /</a>"; 
											
											// меняем класс у клавиши "КАТАЛОГ ПРОДУКЦИИ"
											document.getElementById("section_production").className = "header_block  section_creators";
											// меняем класс у клавиши "ПРОИЗВОДИТЕЛИ"
											document.getElementById("creators_production").className = "second_header  section_creators_active";
										}
									else
										{											
											var array_section1 = section1.split("___!!!___");
											if (array_section1[2] != undefined)                /* условие для промежуточной страницы в производителе - разделы продукции */
												{													
													// меняем класс у клавиши "КАТАЛОГ ПРОДУКЦИИ"
													document.getElementById("section_production").className = "header_block  section_creators";
													// меняем класс у клавиши "ПРОИЗВОДИТЕЛИ"
													document.getElementById("creators_production").className = "second_header  section_creators_active";
													
													
													slovo = array_section1[2];
													
													/* скрипт расшифратор на русский*/
											
													var rus_alphabet = "/ /й/ц/у/к/е/ё/н/г/ш/щ/з/х/ъ/ф/ы/в/а/п/р/о/л/д/ж/э/я/ч/с/м/и/т/ь/б/ю/q/w/e/r/t/y/u/i/o/p/a/s/d/f/g/h/j/k/l/z/x/c/v/b/n/m/Й/Ц/У/К/Е/Ё/Н/Г/Ш/Щ/З/Х/Ъ/Ф/Ы/В/А/П/Р/О/Л/Д/Ж/Э/Я/Ч/С/М/И/Т/Ь/Б/Ю/Q/W/E/R/T/Y/U/I/O/P/A/S/D/F/G/H/J/K/L/Z/X/C/V/B/N/M/0/1/2/3/4/5/6/7/8/9/-/,/./!/+";			/* русский-английский алфавит */
													var eng_alphabet = "/_/y/c/u/k/e/yo/n/g/sh/ss/z/h/qy/f/yy/v/a/p/r/o/l/d/j/ay/ya/ch/s/m/i/t/yi/b/yu/q_/w_/e_/r_/t_/y_/u_/i_/o_/p_/a_/s_/d_/f_/g_/h_/j_/k_/l_/z_/x_/c_/v_/b_/n_/m_/Y/C/U/K/E/YO/N/G/SH/SS/Z/H/QY/F/YY/V/A/P/R/O/L/D/J/AY/YA/CH/S/M/I/T/YI/B/YU/Q_/W_/E_/R_/T_/Y_/U_/I_/O_/P_/A_/S_/D_/F_/G_/H_/J_/K_/L_/Z_/X_/C_/V_/B_/N_/M_/0/1/2/3/4/5/6/7/8/9/--/,/./!/+";		/* псевдо-английский алфавит */

													var slovo = slovo.split('?');

													var summa = ""; 		/* сумматор */

													var l_slovo = slovo;
													l_slovo = l_slovo.length;                		/* длинна фразы */

													rus_alphabet = rus_alphabet.split("/");			/* задаем массивы соответствия через регулярное выражение */
													eng_alphabet = eng_alphabet.split("/");
													
													var l_alphabet = rus_alphabet.length;			/* длинна массива */
														
													for (i = 0; i <= l_slovo-1; i++)
														{
															for (j = 0; j <= l_alphabet; j++)
																{
																	if (slovo[i] == eng_alphabet[j])
																		{
																			var char_bufer = rus_alphabet[j];
																			summa = summa + char_bufer;			/* если значение символа во фразе присутствует заменяем его на соответствующий псевдо-символ */
																		}
																}
														}
													slovo = summa;												/* возвращаем переведенную фразу */
													
													array_section1[2] = slovo;
													
													/* закрываем скрипт  */
													
													$("#catalog").load("../config/menu_creator_navigation.php", "name_company="+array_section1[2]);
													
													// меняем корень в хлебных крошках
													document.getElementById("root").innerHTML = "<a class = 'font_days' onmouseover = \"this.style.cursor = 'pointer'\" onclick = 'look_all_creators()' >производители /</a>"; 
													document.getElementById("navigation_produkciya").innerHTML = "";
													document.getElementById("page_produkciya").innerHTML = "";
													
													document.getElementById("lock1").value = 1;									// закрываем замок 1
													document.getElementById("lock2").value = 0;									// открываем замок 2
													document.getElementById("lock3").value = 0;                                 // открываем замок 3
													move_scroll();
												}
											else
												{	
													// меняем класс у клавиши "КАТАЛОГ ПРОДУКЦИИ"
													document.getElementById("section_production").className = "second_header  section_creators_active";
													// меняем класс у клавиши "ПРОИЗВОДИТЕЛИ"
													document.getElementById("creators_production").className = "header_block  section_creators";
													// меняем корень в хлебных крошках
													document.getElementById("root").innerHTML = "<a class = 'font_days' onmouseover = \"this.style.cursor = 'pointer'\" onclick = 'back_general_catalog()'>каталог /</a>";
										
													load_general_catalog();
												}
										}
										

										
										document.getElementById("navigation_logo_creator").innerHTML = "";
										document.getElementById("navigation_produkciya").innerHTML = "";
										document.getElementById("page_produkciya").innerHTML = "";
										document.getElementById("lock1").value = 1;									// закрываем замок 1
										document.getElementById("lock2").value = 0;									// открываем замок 2
										document.getElementById("lock3").value = 0;                                 // открываем замок 3
										move_scroll();
								}
						}	

				}
			
		}
		
	setInterval("control_url()", 100);
	
	
	
	
	function read_company_active_for_user()																	// функция возврата в раздел второго порядка								
	{      
		unlock();
		/* основная процедура обработки url */
			var current_url = document.location.href;										/* считываем и обрабатываем текущий URL*/
			
			current_url = current_url.split('#');											/* разбиваем адрес на массив подразделов */
			current_url = current_url[1].split('/');										/* разбиваем адрес на массив подразделов */
			
			section1 = current_url[0];														/* определяем раздел первого порядка */
			section2 = current_url[1];														/* определяем раздел второго порядка */
			section3 = current_url[2];														/* определяем раздел третьего порядка */
		/* закрываем процедуру */
		
		document.location.hash = section1 + "/" + section2;
	}
					
	function read_products_for_user()														// функция возврата в раздел третьего порядка								
	{      
		unlock();
		/* основная процедура обработки url */
			var current_url = document.location.href;										/* считываем и обрабатываем текущий URL*/
			
			current_url = current_url.split('#');											/* разбиваем адрес на массив подразделов */
			current_url = current_url[1].split('/');										/* разбиваем адрес на массив подразделов */
			
			section1 = current_url[0];														/* определяем раздел первого порядка */
			section2 = current_url[1];														/* определяем раздел второго порядка */
			section3 = current_url[2];														/* определяем раздел третьего порядка */
			section4 = current_url[3];
		/* закрываем процедуру */
		
		document.location.hash = section1 + "/" + section2 + "/" + section3 + "/" + section4;
	}
					


					
	function unlock()																								// открываем все замки
		{
			document.getElementById("lock1").value = 0;																// открываем замок 1
			document.getElementById("lock2").value = 0;																// открываем замок 2
			document.getElementById("lock3").value = 0;																// открываем замок 3
			document.getElementById("lock5").value = 0;																// открываем замок 5
		}
		
		
		
		// увеличитель цыфр на селекторе страниц
		
	function page_select_class()
		{
			document.getElementById("page_select_" + section4).className = "page_select";        // меняем класс выбранной цыфры на селекторе страниц        
			document.getElementById("page_select_n_" + section4).className = "page_select";        // меняем класс выбранной цыфры на селекторе страниц        
		}
		
		
		
		
	function load_journal_action()																			// загрузка журналов акций
		{
			unlock();
			$("#load_journal_action").load("../config/journal_action.php");
			document.getElementById("on_of_load_journal_action").innerHTML = "<div class = 'rollup' onclick = \' rollup_journal_action() \' ><img src = '../images/rollup.png' style = 'height: 40px;'></div>";
			document.location.hash = "link_tmpl_1";
		}
				
	function unload_journal_action()																		// отгрузка журналов акций
		{
			$("#load_journal_action").load("../load/journal_action_minimum.html");
			document.getElementById("on_of_load_journal_action").innerHTML = "<div class = 'deploy' onclick = 'load_journal_action()' ><img src = '../images/deploy.png' style = 'height: 40px;'></div>";
		}
		
	function rollup_journal_action()
		{
			unload_journal_action();
			move_scroll();
		}
		
		
	function load_diploms()																			// загрузка наград (ныне о нас)
		{
			unlock();
			$("#load_diploms").load("../config/diploms.php");
			document.getElementById("on_of_load_diploms").innerHTML = "<div class = 'rollup' onclick = \' rollup_diploms() \' ><img src = '../images/rollup.png' style = 'height: 40px;'></div>";
			document.location.hash = "link_tmpl_3";
		}
		
	function load_all_diploms()																		// загрузка всех наград
		{
			$("#load_all_diploms").load("../config/load_all_diploms.php");
		}
		
	function upload_all_diploms()																	// отгрузка всех наград
		{
			$("#load_all_diploms").load("../config/upload_all_diploms.php");
			move_scroll();
		}
		
	function unload_diploms()																		// отгрузка наград
		{
			$("#load_diploms").load("../load/diplom_minimum.html");
			document.getElementById("on_of_load_diploms").innerHTML = "<div class = 'deploy' onclick = \' load_diploms() \' ><img src = '../images/deploy.png' style = 'height: 40px;'></div>";
		}
		
	function rollup_diploms()
		{
			unload_diploms();
			move_scroll();
		}
		
		
	

	function goto_confectionery()																	// выводим вид продукции "кондитерка"
		{
			document.getElementById('select_kind_of_products').innerHTML = 'Кондитерcкие <br> изделия'; 
			document.getElementById('kind_of_products').value = 'confectionery'; 
			load_general_catalog();
			move_scroll();
		}
	
	function goto_conservation()																	// выводим вид продукции "консервация"
		{
			document.getElementById('select_kind_of_products').innerHTML = 'Консервация <br> и бакалея';  
			document.getElementById('kind_of_products').value = 'conservation'; 
			load_general_catalog();
			move_scroll();
		}
		
	function goto_new_year_products()																	// выводим вид продукции "Новогодняя продукция"
		{
			document.getElementById('select_kind_of_products').innerHTML = 'Новогодняя<br>родукция';  
			document.getElementById('kind_of_products').value = 'new_year_products'; 
			load_general_catalog();
			move_scroll();
		}
		
		
		
	// загрузка всех производителей в разделе продукции
		
	function look_all_creators()
		{	
			document.location.hash = '#link_tmpl_2___!!!___all_creators';
			unlock();
		}
	function all_creators()
		{
			$("#catalog").load("../config/all_creators.php");
		}
	
	
	
	
	// навигация по разделам страницы
	
	function goto_tmpl_1()
		{	
			unlock();
			document.location.hash = '#link_tmpl_1';
		}
				
	function goto_tmpl_2()
		{
			unlock();
			document.location.hash = '#link_tmpl_2';
			document.getElementById("punkt_tmpl_2_all_creators").style.background = "none";
		}
		
	function goto_news()
		{
			unlock();
			document.location.hash = '#link_tmpl_news';
		}
		
	function goto_tmpl_3()
		{
			unlock();
			document.location.hash = '#link_tmpl_3';
		}
		
	function goto_where_to_buy()
		{
			unlock();
			document.location.hash = '#link_where_to_buy';
		}
		
	function goto_kontakty()
		{
			unlock();
			document.location.hash = '#link_kontakty'; 
		}
		
	function goto_section3()
		{
			
		}
	
		
		
		
var t;		
		
	function move_scroll()	/* функция - двигатель */
		{
			var section1 = document.getElementById('section1').value;					/* считываем раздел первого порядка из буфера */	
			
			var section2 = document.getElementById('section2').value;					/* считываем раздел второго порядка из буфера */
			var section3 = document.getElementById('section3').value;					/* считываем раздел третьего порядка из буфера */
			var section5 = document.getElementById('section5').value;					/* считываем раздел пятого порядка из буфера */
			
			if (section1 != 'tmpl_0')
				{
					var array_section1 = section1.split("___!!!___");
					if (array_section1[1] != undefined)						/* условие для обратной логики - от производителей к разделам */
						{
							var id_tmpl = document.getElementById("tmpl_2");
							if (array_section1[2] != undefined)				/* условие для промежуточной страницы в производителе - разделы продукции */
								{
									slovo = array_section1[2];
													
									/* скрипт расшифратор на русский*/
							
									var rus_alphabet = "/ /й/ц/у/к/е/ё/н/г/ш/щ/з/х/ъ/ф/ы/в/а/п/р/о/л/д/ж/э/я/ч/с/м/и/т/ь/б/ю/q/w/e/r/t/y/u/i/o/p/a/s/d/f/g/h/j/k/l/z/x/c/v/b/n/m/Й/Ц/У/К/Е/Ё/Н/Г/Ш/Щ/З/Х/Ъ/Ф/Ы/В/А/П/Р/О/Л/Д/Ж/Э/Я/Ч/С/М/И/Т/Ь/Б/Ю/Q/W/E/R/T/Y/U/I/O/P/A/S/D/F/G/H/J/K/L/Z/X/C/V/B/N/M/0/1/2/3/4/5/6/7/8/9/-/,/./!/+";			/* русский-английский алфавит */
									var eng_alphabet = "/_/y/c/u/k/e/yo/n/g/sh/ss/z/h/qy/f/yy/v/a/p/r/o/l/d/j/ay/ya/ch/s/m/i/t/yi/b/yu/q_/w_/e_/r_/t_/y_/u_/i_/o_/p_/a_/s_/d_/f_/g_/h_/j_/k_/l_/z_/x_/c_/v_/b_/n_/m_/Y/C/U/K/E/YO/N/G/SH/SS/Z/H/QY/F/YY/V/A/P/R/O/L/D/J/AY/YA/CH/S/M/I/T/YI/B/YU/Q_/W_/E_/R_/T_/Y_/U_/I_/O_/P_/A_/S_/D_/F_/G_/H_/J_/K_/L_/Z_/X_/C_/V_/B_/N_/M_/0/1/2/3/4/5/6/7/8/9/--/,/./!/+";		/* псевдо-английский алфавит */

									var slovo = slovo.split('?');

									var summa = ""; 		/* сумматор */

									var l_slovo = slovo;
									l_slovo = l_slovo.length;                		/* длинна фразы */

									rus_alphabet = rus_alphabet.split("/");			/* задаем массивы соответствия через регулярное выражение */
									eng_alphabet = eng_alphabet.split("/");
									
									var l_alphabet = rus_alphabet.length;			/* длинна массива */
										
									for (i = 0; i <= l_slovo-1; i++)
										{
											for (j = 0; j <= l_alphabet; j++)
												{
													if (slovo[i] == eng_alphabet[j])
														{
															var char_bufer = rus_alphabet[j];
															summa = summa + char_bufer;			/* если значение символа во фразе присутствует заменяем его на соответствующий псевдо-символ */
														}
												}
										}
									slovo = summa;												/* возвращаем переведенную фразу */
									
									array_section1[2] = slovo;
									
									document.getElementById("navigation_logo_creator").innerHTML = "<a> " + array_section1[2] + " /</a>";
								}
						}
					else
						{
							var id_tmpl = document.getElementById(section1);     			/* обращаемся к блоку раздела первого порядка */
						}
					
					id_tmpl = id_tmpl.offsetTop;											/* определяем положение блока по вертикали */
					
					var coord = window.pageYOffset;											/* определяем текущее положение скролла на странице */
					
					if (coord > id_tmpl+15)
						{
							window.scrollBy(0, -20);										/* движемся к заданной точке с низу вверх */
							t = setTimeout("move_scroll()", 1);
						}
					else
						{
							if (coord < id_tmpl-15)
								{
									window.scrollBy(0, 20);										/* движемся к заданной точке с верху вниз */
									t = setTimeout("move_scroll()", 1);
								}
							else
								{
									/* здесь основной замысел навигационной зависимости */
									
									coord == id_tmpl;											/* если пришли к пределу прокрутки прерываем прокрутку */
									
									clearTimeout(t);
									
									if (section2 != 'undefined')								/* если имя раздела второго порядка не пустое добавляем его в URL */				
										{
											section1 = section1 + "/" + section2;
										}
									if (section3 != 'undefined')								/* если имя раздела третьего порядка не пустое добавляем его в URL */				
										{
											if (section5 != 'undefined')								/* если имя раздела третьего порядка не пустое добавляем его в URL */				
												{
													section1 = section1 + "/" + section3 + "/" + section4 + "/" + section5;
												}
											else
												{
													section1 = section1 + "/" + section3 + "/" + section4;
												}
										}

									document.location.hash = "link_" + section1;				/* переходим на заданную ссылку */
								}
						}
				}
			else
				{
					var coord = window.pageYOffset;												/* определяем текущее положение скролла на странице */
					
					if (coord > 30)
						{
							window.scrollBy(0, -20);											/* движемся к заданной точке с низу вверх */
							t = setTimeout("move_scroll()", 1);
						}
					else
						{
							clearTimeout(t);
						}
				}
		}
