var x = 1; 																		// счетчик слайдов по порядку

var slide_interval;																// переменная времени

var interval_s = 1000;															// интервал времени между сдвигами setTimeout("function()", interval_s)

setTimeout("next_slide()", 2000);												// старт при загрузке


function reset_class_slider_selector()	 										// сброс классов active на селекторе
	{
		for (i = 1; i <= 5; i++)
			{
				document.getElementById("slider_selector_" + i).className = "slider_selector";
			}
	}
	
		
var count_slides = 5;															// общее количество слайдов

var counter_slides = [count_slides-1, count_slides, 1, 2, 3];       			// счетчик загружаемых слайдов
var counter_template = [4, 5, 1, 2, 3];											// счетчик циклически чередуемых шаблонов, всего: 5
var position_temlate = [-100, -100, 0, 100, 100];								// массив положений шаблонов
var z_index_temlate = [0, 3, 5, 2, 0];											// массив z-index шаблонов




function for_onload()                                                    	// положение слайдера при загрузке страницы 
	{
		for (i=0; i<=4; i++)
			{
				document.getElementById("slide_" + counter_template[i]).style.left = position_temlate[i] + "%";
				document.getElementById("slide_" + counter_template[i]).style.zIndex = z_index_temlate[i];
				$("#slide_" + counter_template[i]).load("../config/load_slide.php", "load_slide="+counter_slides[i]+"&x="+x);
			}
		document.getElementById("limit_number_slide").value = 0;				// приводим предел прокрутки к нулю для бесконечного цикла
	}
	



function select_slide()															// выбор слайда по селектору
	{
		interval_s = 50;
		for (i=1; i<=5; i++)
			{
				document.getElementById("slide_" + i).style.transition = 0.4 + "s";
			}
		document.getElementById("stop_slider").style.display = "none";
		document.getElementById("play_slider").style.display = "block";
		document.getElementById("limit_number_slide").value = limit_number_slide;
		var number_slide = document.getElementById("number_slide").value;
		clearTimeout(slide_interval);
		if (limit_number_slide < number_slide)
			{
				setTimeout("back_slide()", 10);
			}
		if (limit_number_slide > number_slide)
			{
				setTimeout("next_slide()", 10);
			}
	}
	
	
	
	
function play_slider()
	{
		clearTimeout(slide_interval);
		interval_s = 1000; 
		for (i=1; i<=5; i++)
			{
				document.getElementById("slide_" + i).style.transition = 1 + "s";
			}
		document.getElementById("limit_number_slide").value = 0;					// приводим предел прокрутки к нулю для бесконечного цикла
		setTimeout('next_slide()', 500);
	}
	
	
	
function stop_slider()
	{
		clearTimeout(slide_interval);
		interval_s = 1000; 
		for (i=1; i<=5; i++)
			{
				document.getElementById("slide_" + i).style.transition = 1 + "s";
			}
	}
	


	
function next_slide()														// функция загрузки следующего слайдера
	{	
		slide_interval = setTimeout("next_slide()", 3*interval_s);
		
		var number_slide = document.getElementById("number_slide").value;
		var limit_number_slide = document.getElementById("limit_number_slide").value;
		limit_number_slide--;
		if (number_slide == limit_number_slide)
			{
				clearTimeout(slide_interval);
			}
		
		x++;
		if (x > count_slides)												// просчет нумерации всех слайдов
			{
				x = 1;
			}
		reset_class_slider_selector();
		document.getElementById("slider_selector_" + x).className = "slider_selector_active";
						
		for (i=0; i<=4; i++)												// процедура перелистывания шаблона
			{					
				counter_slides[i]++;										// считаем загрузки слайдов по базе
				if (counter_slides[i] > count_slides)
					{
						counter_slides[i] = 1;
					}
					
				counter_template[i]++;										// считаем шаблоны
				if (counter_template[i] > 5)
					{
						counter_template[i] = 1;
					}
					
				document.getElementById("slide_" + counter_template[i]).style.left = position_temlate[i] + "%";     // сдвигаем шаблоны согласно массиву положений шаблонов
				document.getElementById("slide_" + counter_template[i]).style.zIndex = z_index_temlate[i];			// меняем z-index шаблонов согласно массиву z-index шаблонов
				
				if (i == 0)
					{
						$("#slide_" + counter_template[i]).load("../config/load_slide.php", "load_slide="+counter_slides[i]+"&x="+x);
					}
				if (i == 4)
					{
						$("#slide_" + counter_template[i]).load("../config/load_slide.php", "load_slide="+counter_slides[i]+"&x="+x);
					}
			}
	}
	
	
function back_slide()														// функция загрузки предыдущего слайдера
	{	
		slide_interval = setTimeout("back_slide()", 3*interval_s);
		
		var number_slide = document.getElementById("number_slide").value;
		var limit_number_slide = document.getElementById("limit_number_slide").value;
		limit_number_slide++;
		if (number_slide == limit_number_slide)
			{
				clearTimeout(slide_interval);
			}
		
		x--;
		
		if (x < 1)															// просчет нумерации всех слайдов
			{
				x = count_slides;
			}
		reset_class_slider_selector();
		document.getElementById("slider_selector_" + x).className = "slider_selector_active";
			
		for (i=4; i>=0; i--)												// процедура перелистывания шаблона
			{
				counter_slides[i]--;										// считаем загрузки слайдов по базе
				if (counter_slides[i] < 1)
					{
						counter_slides[i] = count_slides;
					}
					
				counter_template[i]--;																				// считаем шаблоны		
				if (counter_template[i] < 1)
					{
						counter_template[i] = 5; 
					}
					
				document.getElementById("slide_" + counter_template[i]).style.left = position_temlate[i] + "%";		// сдвигаем шаблоны согласно массиву положений шаблонов
				document.getElementById("slide_" + counter_template[i]).style.zIndex = z_index_temlate[i];			// меняем z-index шаблонов согласно массиву z-index шаблонов
				
				if (i == 0)
					{
						$("#slide_" + counter_template[i]).load("../config/load_slide.php", "load_slide="+counter_slides[i]+"&x="+x);
					}
				if (i == 4)
					{
						$("#slide_" + counter_template[i]).load("../config/load_slide.php", "load_slide="+counter_slides[i]+"&x="+x);
					}
			}
	}