<table style = "width: 80%">
	<tr>
		<td>
		
			<div id = "infotext" style = "width: 100%;">
					  
				<p>                                                                                                                                               
				История «Руслады» началась 18 лет назад. Сегодня мы лидеры Урала и Сибири, а в ассортиментной матрице компании – 
				продукция ведущих кондитерских фабрик России и ближнего зарубежья. Сотрудничество по эксклюзивным контрактам, с известными производителями, 
				включая продукцию СТМ. Высокотехнологичное оборудование и опыт профессиональных кондитеров позволяют нашей кондитерской фабрике создавать 
				полюбившиеся требовательному потребителю шедевры 
				сладкой продукции из  натурального сырья под ТМ «Руслада». 
				<br> <br>

				Компания «Руслада» ежегодный оператор Нового Года с гарантированно гибким ценовым подходом к упаковке и ассортиментному наполнению новогодних 
				подарков для VIP, среднего, эконом сегментов. В нашем просторном торговом зале можно лично поучаствовать в дегустациях кондитерской продукции, 
				презентации новинок, ознакомиться с ассортиментом.          <br> <br>                                    

				Мы за сохранность вашего времени – в целях оптимизации, в нашем товарном портфеле ассортимент ТОП-овых производителей бакалеи, овощной, мясной, 
				рыбной, фруктовой и молочной консервации.                                                                                                   
				<br> <br>

				 Расположение нашей базы - на крупной транспортной развязке с удобными подъездными путями, и наличием собственных ж/д путей (3 ж/д ветки).  
				 Отработанный механизм логистического менеджмента позволяет ежесуточно доставлять более 100 тонн продукции в более чем 30 регионов страны и 10 стран зарубежья. 
				 В автопарке компании более 100 автомобилей грузоподъемностью от 1,5 до 20 тонн. 
				 
				 <br> <br>
				 В числе клиентов «Руслады» торговые сети: <br> 
				 </p>
				 <p style = "padding-left: 10px;">
				● Красное&Белое  				<br>
				● Лента							<br>
				● Дикси 						<br>
				● X5 Retail Group 				<br>
				● Монетка 						<br>
				● SPAR							<br>
				● Магнит						<br>
				● Светофор						<br>
				● Ариант						<br>
				● локальные ТС городов России 	<br>

				</p>
			</div>
		
		</td>
	</tr>
</table>

<br><br><br>

<div class = "header_block">НАШИ НАГРАДЫ</div>

<br><br>

<div id = "load_all_diploms">

	<table style = "width: 90%"  cellspacing = "20">
				
		<tr>
		   <td>
				<center>	
				<img src = "../images/diploms/zoloto_garibaldy_hanty.png" style = "width: 90%;" onmouseover = "this.style.opacity = '0.5'" onmouseout = "this.style.opacity = '1'"> 
				</center>
		   </td>
		   <td>
				<center>	
				<img src = "../images/diploms/zoloto_rulet_cherem_hanty.png" style = "width: 90%;" onmouseover = "this.style.opacity = '0.5'" onmouseout = "this.style.opacity = '1'"> 
				</center>
		   </td>
		   <td>
				<center>	
				<img src = "../images/diploms/zoloto_can_tiramisu_hanty.png" style = "width: 90%;" onmouseover = "this.style.opacity = '0.5'" onmouseout = "this.style.opacity = '1'"> 
				</center>
		   </td>
		   <td>
				<center>
				<img src = "../images/diploms/zoloto_bant_cher_hanty.png" style = "width: 90%;" onmouseover = "this.style.opacity = '0.5'" onmouseout = "this.style.opacity = '1'">
				</center>
		   </td>
		   <td>	
				<center>				   
				<img src = "../images/diploms/serebro_zef_hanty.png" style = "width: 90%;" onmouseover = "this.style.opacity = '0.5'" onmouseout = "this.style.opacity = '1'">
				</center>
		   </td>
		   <td>
				<center>	
				<img src = "../images/diploms/serebro_luk_gren_hanty.png" style = "width: 90%;" onmouseover = "this.style.opacity = '0.5'" onmouseout = "this.style.opacity = '1'"> 
				</center>
		   </td>			
		</tr>
		
	</table>
	
	<a onmouseover = "this.style.cursor = 'pointer'" onclick = "load_all_diploms()">
		<img src = "../images/deploy.png" style = "height: 40px;" >
	</a>

</div>

<br>


<?php

// загружаем отправку URL

require_once dirname(__FILE__).'/../load/send_link.php';

?>

<br><br><br>
				