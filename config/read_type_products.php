
<script>
								
	function select_type_product_all_type()
		{
			/* основная процедура обработки url     */

			var current_url = document.location.href;												/* считываем и обрабатываем текущий URL*/
			
			current_url = current_url.split("#");													/* разбиваем адрес на массив подразделов */
			
			if (current_url[1] != undefined)														/* если часть url после знака # не пустое ... */
				{
					current_url = current_url[1].split("/");										/* разбиваем адрес на массив подразделов */
					
					section1 = current_url[0];														/* определяем раздел первого порядка */
					
					section2 = current_url[1];														/* определяем тип продукции */
					section2 = section2.split("___!!!___");
					section2 = section2[0];
					
					section3 = current_url[2];
					section4 = current_url[3];
				}
			if (section3 != undefined)
				{
					document.location.hash = section1 + "/" + section2 + "/" + section3 + "/" + section4;
				}
			else
				{
					document.location.hash = section1 + "/" + section2;
				}
		}
		
		
		
		
		
		
	var button_width = document.getElementById("button_width").clientWidth;						// определяем суммарную ширину ряда клавиш в селекторе типов
	var limit_button_width = document.getElementById("limit_button_width").clientWidth;			// определяем лимит ширины
	
	
			
	function define_button_width()                            								
		{		
			if (button_width > limit_button_width)												// если суммарная ширина превосходит лимит - выводим клавиши влево, вправо 
				{
					document.getElementById("scroll_left").innerHTML = '<img style = "position: absolute; width: 40px; left: -40px;" src = "../images/scroll_left.png" onmousedown = "scroll_left()" onmouseup = "scroll_stop()" onmouseover = "this.src = \'../images/scroll_left_over.png\'" onmouseout = "this.src = \'../images/scroll_left.png\'" >';
					document.getElementById("scroll_right").innerHTML = '<img style = "position: absolute; width: 40px;" src = "../images/scroll_right.png" onmousedown = "scroll_right()" onmouseup = "scroll_stop()"  onmouseover = "this.src = \'../images/scroll_right_over.png\'" onmouseout = "this.src = \'../images/scroll_right.png\'" >';
					/* основная процедура обработки url     */

					var current_url = document.location.href;												/* считываем и обрабатываем текущий URL*/
					
					current_url = current_url.split("#");													/* разбиваем адрес на массив подразделов */
					
					if (current_url[1] != undefined)														/* если часть url после знака # не пустое ... */
						{
							current_url = current_url[1].split("/");										/* разбиваем адрес на массив подразделов */
							
							section1 = current_url[0];														/* определяем раздел первого порядка */
							
							section2 = current_url[1];							
							section2 = section2.split("___!!!___");											/* определяем тип продукции */
							section2 = section2[1];
							
							section3 = current_url[2];				/* определяем раздел третьего порядка для условия перехода со смещением панели */
							
							if (section2 != undefined)
								{
									section2 = section2.split('type');												/* отрываем строковую часть */
									section2 = section2[1];
									section2 = parseInt(section2, 10);
																		
									// процедура смещения клавишного ряда типов продукции
									
									var sum = 0;							
									for (j = 0; j <= section2; j++)
										{
											var button_type_width = $("#type"+j).width();
											if (button_type_width)
												{
													sum = sum + button_type_width + 24;
												}
										}
										
									limit_button_width = $("#limit_button_width").width();
									document.getElementById("button_type_position").value = 0 - sum + limit_button_width;										
									
									current_margin_left = document.getElementById("button_type_margen").value;											// читаем записаное значение ручного сдвига margin-left
																		
									if (sum - limit_button_width > 0)
										{
											if (current_margin_left < 0)
												{
													if (current_margin_left < 0 - sum + limit_button_width)
														{
															if (section3 != undefined)     /* условие перехода на раздел */
																{
																	document.getElementById("button_width").style.marginLeft = 0 - sum + limit_button_width + "px";
																}
															else
																{
																	document.getElementById("button_width").style.marginLeft = current_margin_left;
																}
														}
													else
														{
															document.getElementById("button_width").style.marginLeft = 0 - sum + limit_button_width + "px";
															document.getElementById("button_type_margen").value = 0 - sum + limit_button_width;
														}
												}
											else
												{
													document.getElementById("button_width").style.marginLeft = 0 - sum + limit_button_width + "px";
													document.getElementById("button_type_margen").value = 0 - sum + limit_button_width;
												}
										}
									else
										{
											document.getElementById("button_width").style.marginLeft = 0;
											document.getElementById("button_type_margen").value = 0;
										}
										
								}
							else
								{
									document.getElementById("button_width").style.marginLeft = 0;
									document.getElementById("button_type_margen").value = 0;
								}
								
							
						}
				}
			else
				{
					document.getElementById("button_type_margen").value = 0; 				/* возвращаем буферу обмена значение смещения клавишного ряда 0 */
					document.getElementById("button_width").style.marginLeft = 0 + "px";
				}
		}
		
	setTimeout("define_button_width()", 10);
	
	
	
	
	
	
	var t2;
	
	function scroll_right()																	// движение ряда клавиш вправо
		{
			var difference_button_width = limit_button_width - button_width;
			var current_margin_left = document.getElementById("button_width").style.marginLeft;
			current_margin_left = parseInt(current_margin_left, 10);
			document.getElementById("button_type_margen").value = current_margin_left;
			
			if (current_margin_left > difference_button_width)
				{	
					t2 = setTimeout("scroll_right()", 20);
					document.getElementById("button_width").style.marginLeft = current_margin_left - 10 + "px";
				}			
		}
		
	function scroll_left()																	// движение ряда клавиш влево
		{
			var difference_button_width = limit_button_width - button_width;
			var current_margin_left = document.getElementById("button_width").style.marginLeft;
			current_margin_left = parseInt(current_margin_left, 10);
			document.getElementById("button_type_margen").value = current_margin_left;
			
			if (current_margin_left < 0)
				{
					t2 = setTimeout("scroll_left()", 20);
					document.getElementById("button_width").style.marginLeft = current_margin_left + 10 + "px";
				}
		}
		
	function scroll_stop()
		{
			clearTimeout(t2);
		}
			
</script>



<?php

if (isset($_REQUEST['name_section'])) { $name_section = $_REQUEST['name_section'];}  		// считываем название выбранной в панели продукции
if (isset($_REQUEST['type_product'])) { $type_product = $_REQUEST['type_product'];} 		// считываем тип продукции
if (isset($_REQUEST['name_company'])) { $name_company = $_REQUEST['name_company'];}  		// считываем название производителя
if (isset($_REQUEST['button_type_margen'])) { $button_type_margen = $_REQUEST['button_type_margen'];}  		// считываем смещение клавишного ряда


$db = new PDO('sqlite:'.dirname(__FILE__).DIRECTORY_SEPARATOR.'base.db');

$buf = $db->query('SELECT * FROM section_products WHERE name_section = "'.$name_section.'"');									// читаем классификацию товаров в разделе
$readbuf = $buf->fetchAll();


foreach ($readbuf as $mass)											
	{			
		echo '
					<div id = "scroll_left" style = "position: absolute; left: 0px; top: -5px;">				<!-- кнопка влево -->									
					</div>
					<div id = "limit_button_width" style = "position: relative; overflow: hidden;">
						<div id = "button_width" style = "display: inline-block; vertical-align: top; margin-left: '.$button_type_margen.'px;">
						<NOBR>';
		
		if ($type_product == '')
			{
				echo '<input id = "type0" type = "button" value = "все типы" onclick = "select_type_product_all_type()" class = "type_icon_select" >';
			}
		else
			{
				echo '<input id = "type0" type = "button" value = "все типы"  onclick = "select_type_product_all_type()" class = "type_icon" onmouseover = "this.className = \'type_icon_select\'" onmouseout = "this.className = \'type_icon\'" >';
			}
		
		
		for ($i = 1; $i <= 20; $i++)
			{
				$condition = 0;                       // условие для определения - есть ли данный тип продукции у выбранного производителя
				
				if ($mass['type'.$i] != '')
					{
						echo '
								<script>
								
									function select_type_product_type'.$i.'()
										{
											/* основная процедура обработки url     */
			
											var current_url = document.location.href;												/* считываем и обрабатываем текущий URL*/
											
											current_url = current_url.split("#");													/* разбиваем адрес на массив подразделов */
											
											if (current_url[1] != undefined)														/* если часть url после знака # не пустое ... */
												{
													current_url = current_url[1].split("/");										/* разбиваем адрес на массив подразделов */
													
													section1 = current_url[0];														/* определяем раздел первого порядка */
													
													section2 = current_url[1]
													section2 = section2.split("___!!!___");											/* определяем тип продукции */
													section2 = section2[0];
													
													section3 = current_url[2];
													section4 = 1;
												}
											if (section3 != undefined)
												{
													document.location.hash = section1 + "/" + section2 + "___!!!___type'.$i.'" + "/" + section3 + "/" + section4;
												}
											else
												{
													document.location.hash = section1 + "/" + section2 + "___!!!___type'.$i.'";
												}
										}
											
								</script>';
								
								
								
							if (isset($name_company))                                                                                   /* если выбран производитель(мы находимся в его разделе) - 
																																		отфильтровываем типы продукции в его наличии */
								{
									$buf1 = $db->query('SELECT * FROM production WHERE name_company = "'.$name_company.'"');									// читаем производителей
									$readbuf1 = $buf1->fetchAll();
									
									foreach ($readbuf1 as $mass1)
												{
													if ($mass1['hide'] == '')
														{
															if ($mass1['type'] == $mass['type'.$i])
																{
																	$condition = 1;
																}
														}
												}
												
									if ($condition == 1)																				// если условие определения наличия типа продукции утвердительно ...
										{
											if ($mass1['hide'] == '')
												{
													if ($type_product != $mass['type'.$i])
														{
															echo '<input id = "type'.$i.'" type = "button" value = "'.$mass['type'.$i].'"  onclick = "select_type_product_type'.$i.'()" class = "type_icon" onmouseover = "this.className = \'type_icon_select\'" onmouseout = "this.className = \'type_icon\'" />';
														}
													else
														{
															echo '<input id = "type'.$i.'" type = "button" value = "'.$mass['type'.$i].'"  onclick = "select_type_product_type'.$i.'()" class = "type_icon_select">';
														}
												}
										}
									
								}
							else																										/* если не выбран производитель(мы находимся разделе продукции с логотипами) - 
																																		выводим все типы без исключения */
								{
									if ($mass1['hide'] == '')
										{
											if ($type_product != $mass['type'.$i])
												{
													echo '<input id = "type'.$i.'" type = "button" value = "'.$mass['type'.$i].'"  onclick = "select_type_product_type'.$i.'()" class = "type_icon" onmouseover = "this.className = \'type_icon_select\'" onmouseout = "this.className = \'type_icon\'" >';
												}
											else
												{
													echo '<input id = "type'.$i.'" type = "button" value = "'.$mass['type'.$i].'"  onclick = "select_type_product_type'.$i.'()" class = "type_icon_select">';
												}
										}
								}
								
					}
			}
		echo 					'</NOBR>
							</div>
						</div>
						<div id = "scroll_right" style = "position: absolute; top: -5px; left: 100%;">
						</div>
					';			
	}
	
?>